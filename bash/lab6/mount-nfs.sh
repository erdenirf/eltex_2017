#!/bin/bash
IP=192.168.101.167
DIRSOURCE=/home/nfs
DIRTO=/home/$(whoami)/public
HOSTIP=$(hostname -I | awk '{ print $1 }')

funcdo() {
	FILENAME="Erdeni.txt"
	echo "Erdeni;Tsyrendashiev;${HOSTIP};$(date);$(uptime);" >> ${DIRTO}/${FILENAME}
	echo "${FILENAME} is appended."
}
funcmount() {
	mkdir "${DIRTO}"
	sudo mount -t nfs -O iocharset=utf-8 ${IP}:${DIRSOURCE} ${DIRTO}
	sleep 1
	echo "${IP}:${DIRSOURCE} mounted to ${DIRTO}";
}
funcumount() {
	sudo umount -t nfs -O iocharset=utf-8 ${IP}:${DIRSOURCE} ${DIRTO}
	sleep 1
	echo "${IP}:${DIRSOURCE} is not mounted.";
}
condition() {
	if mount | grep -q ${DIRSOURCE}; then
		echo "${IP}:${DIRSOURCE} mounted to ${DIRTO}";
	else
		echo "${IP}:${DIRSOURCE} is not mounted.";
	fi
}
about() {
	echo "$0 -i <IP address>   Set IP address";
	echo "$0 -s <Source dir>   Set Source directory";
	echo "$0 -t <Dest dir>   Set destination directory";
	echo "$0 -c   Get status: Is mounted?";
	echo "$0 -m   MOUNT ${IP}:${DIRSOURCE} to ${DIRTO}";
	echo "$0 -u   UNMOUT ${IP}:${DIRSOURCE} to ${DIRTO}";
	echo "$0 -d   create file in mounted drive";
}

while getopts "mucdi:s:t:h" opt
do
case $opt in
m) funcmount;;
u) funcumount;;
d) funcdo;;
i) IP=${OPTARG};;
s) DIRSOURCE=${OPTARG};;
t) DIRTO=${OPTARG};;
c) condition;;
h) about;;
*) echo "NFS mounter. For HELP: $0 -h"; exit 0;;
esac
done

if [[ ! $1 ]]; then
	echo "NFS mounter. For HELP: $0 -h";
fi