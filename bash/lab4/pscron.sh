#!/bin/bash

#PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export DISPLAY=:0.0

PROCESS='galculator'
COMMENT='pscron'
LOGNAME="/home/$(whoami)/${COMMENT}.log";
MINUTE='*/60'	#00-59
HOUR='*/24'	#00-23
DAYS='*'	#1-31
MONTHS='*'	#1-12
WEEKDAYS='*'	#1-7

crontab_list() {
	crontab -l | grep -i ${COMMENT}
}
crontab_add() {
	if [[ "${MINUTE}" = "*/60" && "${HOUR}" = "*/24" ]]; then
		echo "Enter minute and hour to add command: $0 -m <minute> -o <hour> -a";
	else
		echo "${PROCESS} adding..."
		crontab -l | grep -i -v ${COMMENT}${PROCESS}${MINUTE}${HOUR} > /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		echo "${MINUTE} ${HOUR} ${DAYS} ${MONTHS} ${WEEKDAYS} $(pwd)/$0 -p ${PROCESS} -i #${COMMENT}${PROCESS}${MINUTE}${HOUR}" >> /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		crontab < /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		rm -rf /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		echo "${MINUTE} ${HOUR} ${DAYS} ${MONTHS} ${WEEKDAYS} ${PROCESS} added successfully."
	fi
}
crontab_remove() {
	if [[ "${MINUTE}" = "*/60" && "${HOUR}" = "*/24" ]]; then
		echo "Enter minute and hour to remove command: $0 -m <minute> -o <hour> -r";
	else
		echo "${PROCESS} removing...";
		crontab -l | grep -i -v ${COMMENT}${PROCESS}${MINUTE}${HOUR} > /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		crontab < /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		rm -rf /tmp/${COMMENT}${PROCESS}${MINUTE}${HOUR}
		echo "${MINUTE} ${HOUR} ${DAYS} ${MONTHS} ${WEEKDAYS} ${PROCESS} removed successfully."
	fi
}
start_process() {
	if [ "$(pstree | grep ${PROCESS})" ]; then
		echo "$(date) ${PROCESS} is already started." >> ${LOGNAME}
	else
		echo "$(date) ${PROCESS} starting..." >> ${LOGNAME}
		sudo -u erdenirf "${PROCESS}"&
		sleep 1
		if [ "$(pstree | grep ${PROCESS})" ]; then
			echo "$(date) ${PROCESS} started successfully." >> ${LOGNAME}
		else
			echo "$(date) ${PROCESS} start is failed." >> ${LOGNAME}
		fi
	fi
}
about() {
	echo -e "$0 -m <minute>\n$0 -o <hour>\n$0 [-d <days>]\n$0 [-n <months>]\n$0 [-w <weekdays>]\n$0 [-p <PROCESS>]\tdefault = ${PROCESS}\n$0 -h HELP\n$0 -m <minute> -o <hour> [-p <PROCESS>] -a CRONTAB ADD process\n$0 -m <minute> -o <hour> [-p <PROCESS>] -r CRONTAB REMOVE process\n$0 -l CRONTAB LIST processes\n$0 [-p <PROCESS>] -i Start process, if not started";
}

while getopts "m:o:d:n:w:p:hlari" opt
do
case $opt in
m) MINUTE=${OPTARG};;
o) HOUR=${OPTARG};;
d) DAYS=${OPTARG};;
n) MONTHS=${OPTARG};;
w) WEEKDAYS=${OPTARG};;
p) PROCESS=${OPTARG};;
h) about;exit;;
l) crontab_list;exit;;
a) crontab_add;exit;;
r) crontab_remove;exit;;
i) start_process;exit;;
*) about; exit;;
esac
done

echo "My Process editor for Crontab. For HELP: $0 -h";
