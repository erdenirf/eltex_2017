#!/bin/bash

FILENAME_SERVICES='/etc/services'
FILENAME_SERVICES_NOGARBAGE='/tmp/services'
FILENAME_BINS='./bin.txt'

FILENAME_SERVICES_NAMES='/tmp/services-names'
FILENAME_PORTS_ALL='/tmp/services-all'
FILENAME_PORTS_OPEN='/tmp/ports'
FILENAME_NMAP_RESULT='/tmp/nmap.txt'
FILENAME_IPTABLES='/etc/iptables.rules'

getports() {
	cat $1 | grep -o -P "\d+\/(tcp|udp)" > $2
}
services() {
	cat ${FILENAME_SERVICES} | grep -o -P "[a-zA-Z][a-zA-Z0-9-.]+" > ${FILENAME_SERVICES_NAMES}
	getports ${FILENAME_SERVICES} ${FILENAME_PORTS_ALL}
}
nmap_open() {
	sudo nmap -sU -sT -F -P0 -oN ${FILENAME_NMAP_RESULT} 127.0.0.1
	getports ${FILENAME_NMAP_RESULT} ${FILENAME_PORTS_OPEN}
}
iptables_clear() {
	sudo iptables -F && echo iptables -F		#clear iptables
}
iptables_drop_default() {
	sudo iptables -P INPUT DROP && echo iptables -P INPUT DROP	#default DROP
}
iptables_add() {
	cat ${FILENAME_BINS} | while read line
	do
		PORT=$(echo $line | grep -o -P "\d+\/(tcp|udp)")
		IFS=/ read -ra DIV <<< "$PORT";
		sudo iptables -A INPUT -p ${DIV[1]} --sport ${DIV[0]} --dport ${DIV[0]} -j ACCEPT && echo iptables -A INPUT -p ${DIV[1]} --sport ${DIV[0]} --dport ${DIV[0]} -j ACCEPT
 		sudo iptables-save > ${FILENAME_IPTABLES} && echo "Save to ${FILENAME_IPTABLES}"
	done
}
iptables_restore() {
	iptables-restore < ${FILENAME_IPTABLES}
}
iptables_list() {
	sudo iptables -L
}

getservices() {
	cat ${FILENAME_SERVICES} | grep -o -P "[a-zA-Z][a-zA-Z0-9-.]+[\W]+\d+\/(tcp|udp)" > ${FILENAME_SERVICES_NOGARBAGE}
}
for_services_names() {
	echo > ${FILENAME_BINS}
	cat ${FILENAME_SERVICES_NOGARBAGE} | while read LINE
	do
		NAME=$(echo $LINE | grep -o -P "[a-zA-Z][a-zA-Z0-9-.]+")
		#echo $NAME >> 2.txt
		IFS=/ read -ra ARRAY <<< "$NAME"
		#echo ${ARRAY[0]}
		if [ "$(which ${ARRAY[0]})" ]; then
			echo "${ARRAY[0]} - installed"
			echo "${LINE}" >> ${FILENAME_BINS}
		fi
	done
}
getservices
for_services_names
iptables_clear
iptables_drop_default
iptables_add
