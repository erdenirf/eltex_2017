# Task 5 bash #

This source for Educational Only

### What is this repository for? ###

* Eltex Nstu Summer 2017

### Bash Scripts ###

* Ubuntu 16.10
* bash

### How do I get set up? ###

* Linux administrations

### Task ###

Написать скрипт разбора логов, скрипт запрашивает имя процесса и ищет для него логи, вводит их на экран за указанный промежуток времени. 

Скрипт по очереди проверяет наличие файла с именем процесса в папке /var/log/, если файл найден — проверяет его на наличие в нем требуемой информации, если его нет, проверяет наличие папки с имененм процесса, при нахождении папки скрипт просматривает все файлы в этой папке на предмет нахождения в ней требуемой информации, если не найдена папка, то информация ищется в syslog.

### Author ###

* Erdeni Tsyrendashiev
