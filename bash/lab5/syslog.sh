#!/bin/bash

if [[ "$3" ]]; then
	PROC=$1      #process name
	DATEBEGIN=($(date --date="$2" +%s))    #convert to UNIX seconds
	DATEEND=($(date --date="$3" +%s))    #convert to UNIX seconds
else
	echo "Error. Usage: $0 <process name> <date begin> <date end>. Date format usage: date --help";
	exit 1;
fi

FILENAME="/var/log/$PROC.1 /var/log/$PROC /var/log/$PROC.log.1 /var/log/$PROC.log"
for filename in $FILENAME
do
	if [[ -f $filename ]]; then
		EXIST_FILENAMES="$EXIST_FILENAMES $filename"
	fi
done
if [[ ! $EXIST_FILENAMES ]]; then
	cd /var/log/$PROC/
	EXIST_FILENAMES=($(ls --ignore=*.gz))
	if [[ ! $EXIST_FILENAMES ]]; then
		echo "No such files or catalog exist.";
		exit 1;
	fi
fi

func_syslog() {
	REGEXP0="(Sun|Mon|Tue|Wed|Thu|Fri|Sat) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d+ \d+:\d{2}:\d{2}(| \d{4})"	#Fri Oct 23 12:45:59[ 2016]
	REGEXP1="(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)  \d+ \d+:\d{2}:\d{2}"	#Aug  8 12:30:23
	REGEXP2="\d{4}-\d{2}-\d{2} \d+:\d{2}:\d{2}"			#2017-07-23 23:12:10
	SYSEXPR="$REGEXP0|$REGEXP1|$REGEXP2"
	TEMPFILE="/tmp/$PROC.log.date"

	cat ${EXIST_FILENAMES[@]} | grep -o -P "$SYSEXPR" | uniq > $TEMPFILE
	while IFS='' read -r line || [[ -n "$line" ]]; do
		DATESEC=($(date --date="$line" +%s))
		if [[ "$DATEBEGIN" -le "$DATESEC" && "$DATESEC" -le "$DATEEND" ]]; then
			cat ${EXIST_FILENAMES[@]} | grep "$line"
		fi
	done < $TEMPFILE
}

func_syslog
