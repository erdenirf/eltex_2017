#!/bin/bash

SOUNDFILE='/usr/share/sounds/ubuntu/ringtones/Alarm clock.ogg'
PLAYER='play'
COMMENT='budilnik'
MINUTE='*/60'	#00-59
HOUR='*/24'	#00-23
DAYS='*'	#1-31
MONTHS='*'	#1-12
WEEKDAYS='*'	#1-7

crontab_list() {
	crontab -l | grep -i ${COMMENT}
}
crontab_add() {
	if [[ "${MINUTE}" = "*/60" && "${HOUR}" = "*/24" ]]; then
		echo "Enter minute and hour to add alarm: $0 -m <minute> -o <hour> -a";
	else
		echo "Alarm adding..."
		crontab -l | grep -i -v ${COMMENT}${MINUTE}${HOUR} > ${COMMENT}.tmp
		echo "${MINUTE} ${HOUR} ${DAYS} ${MONTHS} ${WEEKDAYS} ${PLAYER} \"${SOUNDFILE}\" #${COMMENT}${MINUTE}${HOUR}" >> ${COMMENT}.tmp
		crontab < ${COMMENT}.tmp
		rm -rf ${COMMENT}.tmp
		echo "Alarm added successfully."
	fi
}
crontab_remove() {
	if [[ "${MINUTE}" = "*/60" && "${HOUR}" = "*/24" ]]; then
		echo "Enter minute and hour to remove alarm: $0 -m <minute> -o <hour> -r";
	else
		echo "Alarm removing...";
		crontab -l | grep -i -v ${COMMENT}${MINUTE}${HOUR} > ${COMMENT}.tmp
		crontab < ${COMMENT}.tmp
		rm -rf ${COMMENT}.tmp
		echo "Alarm removed successfully."
	fi
}

about() {
	echo -e "$0 -m <minute>\n$0 -o <hour>\n$0 -d <days>\n$0 -n <months>\n$0 -w <weekdays>\n$0 -p <player>\n$0 -s <soundfile>\n$0 -h HELP\n$0 -m <minute> -o <hour> -a CRONTAB ADD alarm\n$0 -m <minute> -o <hour> -r CRONTAB REMOVE alarm\n$0 -l CRONTAB LIST alarms";
}

while getopts "m:o:d:n:w:p:s:hlar" opt
do
case $opt in
m) MINUTE=${OPTARG};;
o) HOUR=${OPTARG};;
d) DAYS=${OPTARG};;
n) MONTHS=${OPTARG};;
w) WEEKDAYS=${OPTARG};;
p) PLAYER=${OPTARG};;
s) SOUNDFILE=${OPTARG};;
h) about;exit;;
l) crontab_list;exit;;
a) crontab_add;exit;;
r) crontab_remove;exit;;
esac
done

echo "My Alarm for Crontab. For HELP: $0 -h";
