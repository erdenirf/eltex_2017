#!/bin/bash
# ipcalc.sh: Вычисление диапазона IP-адресов при заданной маске подсети

IP=(0 0 0 0)
MASK=(0 0 0 0)
CIDR=('0.0.0.0' 32)
WILDCARD=(0 0 0 0)
NETWORK=(0 0 0 0)
HOSTMIN=(0 0 0 0)
HOSTMAX=(0 0 0 0)
BROADCAST=(0 0 0 0)
HOSTS=0
CLASS='X'

echo_binary_ip() {
	echo "obase=2;$1" | bc | awk '{printf "%08d.", $0}';
	echo "obase=2;$2" | bc | awk '{printf "%08d.", $0}';
	echo "obase=2;$3" | bc | awk '{printf "%08d.", $0}';
	echo "obase=2;$4" | bc | awk '{printf "%08d\n", $0}';
}
echo_labeled_ip() {
	echo -ne "$1:\t$2.$3.$4.$5\t";
	echo_binary_ip $2 $3 $4 $5;
}
echo_labeled_network() {
	echo -ne "$1:\t$2.$3.$4.$5/$6\t";
	echo_binary_ip $2 $3 $4 $5;
}
calc_class() {
	if [[ "${IP[0]}" -ge "1" && "${IP[0]}" -le "126" ]]
	then
		CLASS='A'
	elif [[ "${IP[0]}" -ge "128" && "${IP[0]}" -le "191" ]]
	then
		CLASS='B'
	elif [[ "${IP[0]}" -ge "192" && "${IP[0]}" -le "223" ]]
	then
		CLASS='C'
	elif [[ "${IP[0]}" -ge "224" && "${IP[0]}" -le "239" ]]
	then
		CLASS='D'
	elif [[ "${IP[0]}" -ge "240" && "${IP[0]}" -le "254" ]]
	then
		CLASS='E'
	fi
}
calc_wildcard() {
	for i in 0 1 2 3
	do
		WILDCARD[$i]=$((255 - ${MASK[$i]}))
	done
}
calc_hosts_net() {
	HOSTS=-1
	for i in 3 2 1 0
	do
		mul=$((3 - $i))
		HOSTS=$(( $HOSTS + 256 ** $mul * ${WILDCARD[$i]} ))
	done
}
calc_network() {
	for i in 0 1 2 3
	do
		NETWORK[$i]=$((${IP[$i]} & ${MASK[$i]}))
	done
}
calc_hostmin() {
	CF=1
	for i in 3 2 1 0
	do
		HOSTMIN[$i]=$((${NETWORK[$i]} + $CF))
		CF=$(((${HOSTMIN[$i]} & 256) >> 8))
		HOSTMIN[$i]=$((${HOSTMIN[$i]} & 255))
	done
}
calc_hostmax() {
	CF=-1
	for i in 3 2 1 0
	do
		HOSTMAX[$i]=$((${NETWORK[$i]} + ${WILDCARD[$i]} + $CF))
		CF=$(((${HOSTMAX[$i]} & 256) >> 8))
		HOSTMAX[$i]=$((${HOSTMAX[$i]} & 255))
	done
}
calc_broadcast() {
	CF=0
	for i in 3 2 1 0
	do
		BROADCAST[$i]=$((${NETWORK[$i]} + ${WILDCARD[$i]} + $CF))
		CF=$(((${BROADCAST[$i]} & 256) >> 8))
		BROADCAST[$i]=$((${BROADCAST[$i]} & 255))
	done
}
convert_mask_to_cidr() {
	cidr=0
	count=0
	for i in 3 2 1 0
	do
		temp=${MASK[$i]}
		while [[ "$temp" != 0 ]]
		do
			mod=$(($temp % 2))
			temp=$(($temp / 2))
			massiv[$count]=$mod
			count=$(($count + 1))
		done
	done
	while [[ "$count" != 0 ]]
	do
		count=$(($count - 1))
		if [[ "${massiv[$count]}" == 1 ]]
		then
			cidr=$(($cidr + 1))
		else
			break
		fi
	done
	CIDR[1]=$cidr
}
convert_cidr_to_netmask() {
	div=$((${CIDR[1]} / 8))
	while [[ "$div" != 0 ]]
	do
		div=$(($div - 1))
		MASK[$div]=255
	done
	div1=$((${CIDR[1]} / 8))
	mod=$((${CIDR[1]} % 8))
	temp=128
	while [[ "$mod" != 0 ]]
	do
		MASK[$div1]=$((${MASK[$div1]} + $temp))
		temp=$(($temp / 2))
		mod=$(($mod - 1))
	done
}

if [[ "$2" ]]		#have netmask
then
	IFS=. read -ra MASK <<< "$2";
	if [[ "$1" ]]		#ip-address have
	then
		IFS=/ read -ra CIDR <<< "$1";
		IFS=. read -ra IP <<< "${CIDR[0]}";
		convert_mask_to_cidr
	fi
else			#have not netmask
	if [[ "$1" ]]		#cidr have
	then
		IFS=/ read -ra CIDR <<< "$1";
		IFS=. read -ra IP <<< "${CIDR[0]}";
		convert_cidr_to_netmask
	else
		echo -e "Usage 1: $0 <IP-address> <Mask>\n\tExample: $0 192.167.1.23 255.255.128.0"
		echo -e "Usage 2: $0 <IP-address/CIDR>\n\tExample: $0 192.167.1.23/17"
		exit 1
	fi
fi

calc_wildcard
calc_hosts_net
calc_network
calc_hostmin
calc_hostmax
calc_broadcast
calc_class

echo_labeled_ip "Address" ${IP[0]} ${IP[1]} ${IP[2]} ${IP[3]}
echo_labeled_ip "Netmask" ${MASK[0]} ${MASK[1]} ${MASK[2]} ${MASK[3]}
echo -e "CIDR:     \t"${CIDR[1]}
echo_labeled_ip "Wildcard" ${WILDCARD[0]} ${WILDCARD[1]} ${WILDCARD[2]} ${WILDCARD[3]}
echo '=>'
echo_labeled_network "Network" ${NETWORK[0]} ${NETWORK[1]} ${NETWORK[2]} ${NETWORK[3]} ${CIDR[1]}
echo_labeled_ip "HostMin" ${HOSTMIN[0]} ${HOSTMIN[1]} ${HOSTMIN[2]} ${HOSTMIN[3]}
echo_labeled_ip "HostMax" ${HOSTMAX[0]} ${HOSTMAX[1]} ${HOSTMAX[2]} ${HOSTMAX[3]}
echo_labeled_ip "Broadcast" ${BROADCAST[0]} ${BROADCAST[1]} ${BROADCAST[2]} ${BROADCAST[3]}
echo -e "Hosts/Net:\t$HOSTS\t\tClass $CLASS"
