#!/bin/bash

REP='origin'
BRANCH='master'

about() {
 echo -e "\tgit status:\t$0 -s";
 echo -e "\tgit add:\t$0 -a <file>";
 echo -e "\tgit reset HEAD:\t$0 -r <file>";
 echo -e "\tgit checkout:\t$0 -u <file>";
 echo -e "\tgit commit -m <string>:\t$0 -c <string>";
 echo -e "\tgit commit -a -m \$date:\t$0 -f";
 echo -e "\tset branch:\t$0 -z <string>. Default: master.";
 echo -e "\tset repository:\t$0 -o <string>. Default: origin.";
 echo -e "\tgit push \$repository \$branch:\t$0 -p -o <repository> -z <branch>. Default: git push origin master.";
 echo -e "\tgit branch <new>:\t$0 -b -z <new>";
 echo -e "\tgit merge <new>:\t$0 -m -z <new>";
 echo -e "\tgit pull <repository>:\t$0 -l -o <repository>";
 echo -e "\tgit clone <git url>:\t$0 -n <git url>";
 echo -e "\tgit remote <git url:\t$0 -t <git url>";
 echo -e "\tgit rm <file>:\t$0 -d <file>";
 echo -e "\tgit fetch <repository>:\t$0 -e <repository>";
 echo -e "\tgit log:\t$0 -g";
 echo -e "\tgit diff:\t$0 -i";
} 

while getopts "sa:r:u:c:fhz:o:pbmln:t:d:e:gi" opt
do
case $opt in
s) git status;;
a) git add ${OPTARG};;
r) git reset HEAD ${OPTARG};;
u) git checkout ${OPTARG};;
c) git commit -m ${OPTARG};;
f) git commit -a -m "$(date)";;
h) about;;
z) BRANCH=${OPTARG};;
o) REP=${OPTARG};;
p) git push ${REP} ${BRANCH};;
l) git pull ${REP};;
b) git branch ${BRANCH};;
m) git merge ${BRANCH};;
n) git clone ${OPTARG};;
t) git remote ${OPTARG};;
d) git rm ${OPTARG};;
e) git fetch ${OPTARG};;
g) git log;;
i) git diff;;
esac
done

echo "This script is for git. Help: $0 -h";
