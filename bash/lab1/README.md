# Task 1 bash #

This source for Educational Only

### What is this repository for? ###

* Eltex Nstu Summer 2017

### Bash Scripts ###

* Ubuntu 16.10
* bash

### How do I get set up? ###

* Linux administrations

### Task ###

Скриптом на BASH, создать 5 папок в указанной, в каждой из 5ти папок создать еще 10 подпапок, и в каждой из этих подпапок создать 20 пустых файлов. 

Модернизировать скрипт, добавив в него возможность пользователю выбирать место расположения папок, их количество, шаблон имени и т.д.

### Author ###

* Erdeni Tsyrendashiev
