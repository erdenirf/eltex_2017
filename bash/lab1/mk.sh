$ cat getopts.sh
#!/bin/bash
CD="./";
PC=5;
AC=10;
FC=20;
PN="dir";
AN="subdir";
FN="file";

while getopts "d:a:c:f:x:y:z:lr:" opt
do
case $opt in
d) CD=$OPTARG;;
a) PC=$OPTARG;;
c) AC=$OPTARG;;
f) FC=$OPTARG;;
x) PN=$OPTARG;;
y) AN=$OPTARG;;
z) FN=$OPTARG;;
l) LSTREE=1;;
r) RMLIST=$OPTARG;;
esac
done 

echo "Settings from command args";
echo "-d $CD   (change directory)";
echo "-a $PC   (count of dirs)";
echo "-c $AC   (count of subdirs)";
echo "-f $FC   (count of files)";
echo "-x $PN   (names of dirs)";
echo "-y $AN   (names of subdirs)";
echo "-z $FN   (names of files)";
echo "-l    (show dirs and subdirs)"
echo "-r    (remove dirs and files)"

declare -a DNAMES	#dirs names
i=1;
while [ $i -le $PC ]
do
	j=1;
	while [ $j -le $AC ]
	do
		k=$(( i * AC + j ))
		DNAMES[$k]=$CD/$PN$i/$AN$j/
		j=$(( j + 1 ))
	done
	i=$(( i + 1 ))
done
declare -a FNAMES	#files names
i=1;
while [ $i -le $FC ]
do
	FNAMES[$i]=$FN$i
	i=$(( i + 1 ))
done


if [[ $RMLIST ]]
then
	rm -rf $CD/$RMLIST
else
	for subfolder in ${DNAMES[@]}
	do
		mkdir -p $subfolder
		for subfile in ${FNAMES[@]}
		do
			touch $subfolder/$subfile
		done
	done
fi

if [[ $LSTREE ]]
then
	tree $CD
fi

