#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>

int open_created_socket_tcp_inet();
int bind_socket_tcp_inet(int sockfd, uint16_t port);
void listen_socket(int sockfd, int query_size);
int open_socket_accept(int sockfd);
int read_write_socket_tcp(int sockfd);
char ** ssplit(const char * string, const char * delim, size_t * result_size);
char * sjoin(char ** array, size_t count, const char * delim);
char * get_request_uri(char * request_header);
char * get_file_type(const char * request_uri);
size_t get_content_length(const char * request_uri);
char * get_content_type(const char * request_uri);
char * get_http_header(const char * request_uri);
char * get_http_body(const char * request_uri);

#define MAX_HTTP_HEADER_LENGTH 20000

int main(int argc, char * argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Error. Argument not found, cmd line haven't argv[1]. Usage: %s <port>\n", argv[0]);
    exit(1);
  }
  int serverfd = open_created_socket_tcp_inet();
  if (serverfd < 0) {
    exit(2);
  }
  int portno = atoi(argv[1]);
  int bind_code = bind_socket_tcp_inet(serverfd, portno);
  if (bind_code < 0) {
    exit(3);
  }
  const int QUERY_SIZE = 5;
  listen_socket(serverfd, QUERY_SIZE);

  while (1)  //цикл извлечения запросов на подключение из очереди
    {
      int accepted_fd = open_socket_accept(serverfd);
      if (accepted_fd < 0) {
	exit(4);
      }
      pid_t pid = fork();
      if (0 > pid) {
	perror("fork");
      }
      if (0 == pid) {  // child
	close(serverfd);
	read_write_socket_tcp(accepted_fd);
	exit(0);
      }
      if (0 < pid) {   //parent
	close(accepted_fd);
      }
    } /* end while */

  close(serverfd);
  return 0;
}

/**
* Socket low level read and write
*/
int read_write_socket_tcp(int sockfd) {
    int bytes_recv = 0;
    char buf[MAX_HTTP_HEADER_LENGTH];
    bytes_recv = read(sockfd, &buf[0], sizeof(buf));
    if (bytes_recv < 0) {
      perror("read");
      return -1;
    }
    char * request_uri = NULL;
    request_uri = get_request_uri(&buf[0]);
    char * header_response = NULL;
    header_response = get_http_header(request_uri);
    char * http_body = NULL;
    http_body = get_http_body(request_uri);
    write(sockfd, header_response, strlen(header_response));
    write(sockfd, http_body, strlen(http_body));
    write(sockfd, "\r\n", 2);
    free(request_uri);
    request_uri = 0;
    free(header_response);
    header_response = 0;
    free(http_body);
    http_body = 0;
    return 0;
}

/**
 * Get file type
 */
char * get_file_type(const char * request_uri) {
  char ** string_array = NULL;
  size_t sarray_len = 0;
  string_array = ssplit(request_uri, ".", &sarray_len);
  return string_array[sarray_len-1];
  char * result = NULL;
  result=calloc(strlen(string_array[sarray_len-1]), sizeof(char));
  strncpy(result, string_array[sarray_len-1], strlen(string_array[sarray_len-1]));
  for (int i=0; i!=sarray_len; ++i) {
    free(string_array[i]);
    string_array[i] = 0;
  }
  free(string_array);
  string_array = 0;
  return result;
}

/**
 * Get content length
 */
size_t get_content_length(const char * request_uri) {
  const int BUFSIZE = strlen(request_uri)+1;
  char filename[BUFSIZE];
  strcpy(filename, ".");
  strncat(filename, request_uri, strlen(request_uri));
  int readfd = open(filename, O_RDONLY, 0);
  if (readfd < 0) {
    fprintf(stderr, "%s ", filename);
    perror("open");
    return -1;
  }
  off_t currentPos = lseek(readfd, (size_t)0, SEEK_CUR);
  size_t size = lseek(readfd,(size_t)0,SEEK_END);
  lseek(readfd, currentPos, SEEK_SET);
  close(readfd);
  return size;
}

/**
 * Get http header; charset=UTF-8
 */

char * get_http_header(const char * request_uri) {
  char * header = NULL;
  header = calloc(MAX_HTTP_HEADER_LENGTH, sizeof(char));
  strcpy(header, "HTTP/1.1 200 OK\n");
  strcat(header, "Cache-Control: no-store, no-cache, must-revalidate\n");
  strcat(header, "Connection: keep-alive\n");
  strcat(header, "Content-Encoding: identity\n");
  strcat(header, "Transfer-Encoding: identity\n");
  size_t CONTENT_LENGTH = get_content_length(request_uri);
  const int BUFSIZE = 255;
  char string_content_length[BUFSIZE];
  snprintf(string_content_length, BUFSIZE, "Content-Length: %u\n", CONTENT_LENGTH);
  strcat(header, string_content_length);
  char * content_type = NULL;
  content_type = get_content_type(request_uri);
  char string_content_type[BUFSIZE];
  snprintf(string_content_type, BUFSIZE, "Content-Type: %s\n", content_type);
  strcat(header, string_content_type);
  strcat(header, "Expires: Thu, 19 Nov 1981 08:52:00 GMT\n");
  strcat(header, "Pragma: no-cacheServer: erdeni\n");
  strcat(header, "\n");
  return header;
}

/**
 * get http body bytes
 */
char * get_http_body(const char * request_uri) {
  size_t CONTENT_LENGTH = get_content_length(request_uri);
  char * block = NULL;
  block = calloc(CONTENT_LENGTH, sizeof(char));
  memset(block, 0, CONTENT_LENGTH);
  const int BUFSIZE = strlen(request_uri)+1;
  char filename[BUFSIZE];
  strcpy(filename, ".");
  strncat(filename, request_uri, strlen(request_uri));
  int readfd = open(filename, O_RDONLY, 0);
  if (readfd < 0) {
    fprintf(stderr, "%s ", filename);
    perror("open");
    return NULL;
  }
  int bytes_recv = read(readfd, block, CONTENT_LENGTH);
  if (bytes_recv < 0) {
    perror("read");
    return NULL;
  }
  close(readfd);
  return block;
}

/**
 * Create AF_INET socket with same port and return file descriptor 
 */
int open_created_socket_tcp_inet() {
    // AF_INET     - сокет Интернета
    // SOCK_STREAM  - потоковый сокет (с установкой соединения)
    // 0 - по умолчанию выбирается TCP протокол
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // ошибка при создании сокета
	if (sockfd < 0) {
	  perror("socket");
	  return -1;
	}
	return sockfd;
}

/**
 * Bind AF_INET socket with same port to Local address
 */
int bind_socket_tcp_inet(int sockfd, uint16_t port) {
  // Шаг 2 - связывание сокета с локальным адресом
  struct sockaddr_in serv_addr; // структура сокета сервера
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    serv_addr.sin_port = htons(port);
    // вызываем bind для связывания
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("bind");
      return -1;
    }
    return 0;
}

/**
 * listen socket with same query size
 */
void listen_socket(int sockfd, int query_size) {
  listen(sockfd, query_size);
}

/**
 * Get socket file descriptor who accept from AF_INET socket
 */
int open_socket_accept(int sockfd) {
  struct sockaddr_in cli_addr; // структура сокета клиента
  socklen_t clilen;       // размер структуры
  int result_socket = 0;      // файловый дестриптор результата
  clilen = sizeof(cli_addr);
  result_socket = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (result_socket < 0) {
    perror("accept");
    return -1;
  }
  return result_socket;
}

/**
 * Get REQUEST_URI. Example: /public_html/index.html  /public_html/css/style.css
 */
char * get_request_uri(char * request_header) {
    char * search = NULL;
    const int BUFSIZE = 255;
    char * request_uri = NULL;
    request_uri = calloc(BUFSIZE, sizeof(char));
    strcpy(request_uri, "");
    search = strstr(request_header, "GET /");
    if (search != NULL) {
      char * search_end = NULL;
      search_end = strstr(request_header, " HTTP");
      strncat(request_uri, search+4, search_end-search-4);
    }
    return request_uri;
}

/*
 * Разбить строку на массив строк по указанной строке-разделителю
 */
char ** ssplit(const char * string, const char * delim, size_t * result_size) {
  char ** result = NULL;
  size_t asize = 0;
  char * temp = NULL;
  temp=(char*)malloc((strlen(string)+1)*sizeof(char));
  strcpy(temp, string);
  char * pch = strtok (temp, delim);
  while (pch != NULL) {
    ++asize;
    result=(char**)realloc(result, asize*sizeof(char*));
    result[asize-1]=(char*)malloc((strlen(pch)+1)*sizeof(char));
    strcpy(result[asize-1], pch);
    pch = strtok(NULL, delim);
  }
  free(temp);
  temp=0;
  *result_size = asize;
  return result;
}

/*
 * Соединить строки и поставить между ними указанный разделитель
 */
char * sjoin(char ** array, size_t count, const char * delim) {
  char * result = NULL;
  int len = strlen(array[0])+1;
  result=(char*)malloc(len*sizeof(char));
  strcpy(result, array[0]);
  for (int i=1; i<count; ++i) {
    len+=strlen(array[i])+strlen(delim);
    result=(char*)realloc(result, len*sizeof(char));
    strncat(result, delim, strlen(delim));
    strncat(result, array[i], strlen(array[i]));
  }
  return result;
}

/**
* Get content type. Example: text/html, application/javascript, image/png
*/
char * get_content_type(const char * request_uri) {
  char * filetype = NULL;
  filetype = get_file_type(request_uri);
  const int BUFSIZE = 255;
  char * result = NULL;
  result = calloc(BUFSIZE, sizeof(char));
  if (strstr(".css.csv.html.ics", filetype)!=NULL) {
    strcpy(result, "text/");
    if (strstr(".htm.html", filetype)!=NULL) {
      strcat(result, "html");
    }
    else if (strstr(".ics", filetype)!=NULL) {
      strcat(result, "calendar");
    }
    else {
      strcat(result, filetype);
    }
  }
  else if (strstr(".gif.ico.jpg.jpeg.png.svg.tiff.webp", filetype)!=NULL) {
    strcpy(result, "image/");
    if (strstr(".ico", filetype)!=NULL) {
      strcat(result, "x-icon");
    }
    else if (strstr(".jpeg.jpg", filetype)!=NULL) {
      strcat(result, "jpeg");
    }
    else if (strstr(".svg", filetype)!=NULL) {
      strcat(result, "svg+xml");
    }
    else if (strstr(".tif.tiff",filetype)!=NULL) {
      strcat(result, "tiff");
    }
    else {
      strcat(result, filetype);
    }
  }
  else if (strstr(".abw.arc.azw.bin.bz.bz2.csh.doc.eot.epub.jar.js.json.mpkg.odp.ods.odt.ogx.pdf.ppt.rar.rtf.sh.swf.tar.vsd.xhtml.xls.xml.xul.zip.7z",filetype)!=NULL){
    strcpy(result, "application/");
    if (strstr(".abw", filetype)!=NULL) {
      strcat(result, "x-abiword");
    }
    else if (strstr(".arc.bin", filetype)!=NULL) {
      strcat(result, "octet-stream");
    }
    else if (strstr(".azw", filetype)!=NULL) {
      strcat(result, "vnd.amazon.ebook");
    }
    else if (strstr(".bz", filetype)!=NULL) {
      strcat(result, "x-bzip");
    }
    else if (strstr(".bz2", filetype)!=NULL) {
      strcat(result, "x-bzip2");
    }
    else if (strstr(".csh", filetype)!=NULL){
      strcat(result, "x-csh");
    }
    else if (strstr(".doc", filetype)!=NULL){
      strcat(result, "msword");
    }
    else if (strstr(".eot", filetype)!=NULL){
      strcat(result, "vnd.ms-fontobject");
    }
    else if (strstr(".epub", filetype)!=NULL){
      strcat(result, "epub+zip");
    }
    else if (strstr(".jar", filetype)!=NULL){
      strcat(result, "java-archive");
    }
    else if (strstr(".js", filetype)!=NULL){
      strcat(result, "javascript");
    }
    else if (strstr(".mpkg", filetype)!=NULL){
      strcat(result, "vnd.apple.installer+xml");
    }
    else if (strstr(".odp", filetype)!=NULL){
      strcat(result, "vnd.oasis.opendocument.presentation");
    }
    else if (strstr(".ods", filetype)!=NULL){
      strcat(result, "vnd.oasis.opendocument.spreadsheet");
    }
    else if (strstr(".odt", filetype)!=NULL){
      strcat(result, "vnd.oasis.opendocument.text");
    }
    else if (strstr(".ogx", filetype)!=NULL){
      strcat(result, "ogg");
    }
    else if (strstr(".ppt", filetype)!=NULL){
      strcat(result, "vnd.ms-powerpoint");
    }
    else if (strstr(".rar", filetype)!=NULL){
      strcat(result, "x-rar-compressed");
    }
    else if (strstr(".sh", filetype)!=NULL){
      strcat(result, "x-sh");
    }
    else if (strstr(".swf", filetype)!=NULL){
      strcat(result, "x-shockwave-flash");
    }
    else if (strstr(".tar", filetype)!=NULL){
      strcat(result, "x-tar");
    }
    else if (strstr(".vsd", filetype)!=NULL){
      strcat(result, "vnd.visio");
    }
    else if (strstr(".xhtml", filetype)!=NULL){
      strcat(result, "xhtml+xml");
    }
    else if (strstr(".xls", filetype)!=NULL){
      strcat(result, "vnd.ms-excel");
    }
    else if (strstr(".xul", filetype)!=NULL){
      strcat(result, "vnd.mozilla.xul+xml");
    }
    else if (strstr(".7z", filetype)!=NULL) {
      strcat(result, "x-7z-compressed");
    }
    else {
      strcat(result, filetype);
    }
  }
  if (strstr(".otf.ttf.woff2", filetype)!=NULL) {
    strcpy(result, "font/");
    strcat(result, filetype);
  }
  if (strstr(".avi.mpeg.ogv.ts.webm.3gp.3g2", filetype)!=NULL) {
    strcpy(result, "video/");
    if (strstr(".avi", filetype)!=NULL) {
      strcat(result, "x-msvideo");
    }
    else if (strstr(".ogv", filetype)!=NULL){
      strcat(result, "ogg");
    }
    else if (strstr(".ts", filetype)!=NULL){
      strcat(result, "vnd.dlna.mpeg-tts");
    }
    else if (strstr(".3gp", filetype)!=NULL){
      strcat(result, "3gpp");
    }
    else if (strstr(".3g2", filetype)!=NULL){
      strcat(result, "3gpp2");
    }
    else {
      strcat(result, filetype);
    }
  }
  if (strstr(".aac.midi.oga.wav.weba", filetype)!=NULL) {
    strcpy(result, "audio/");
    if (strstr(".mid.midi", filetype)!=NULL) {
      strcat(result, "midi");
    }
    else if (strstr(".oga", filetype)!=NULL){
      strcat(result, "ogg");
    }
    else if (strstr(".wav", filetype)!=NULL){
      strcat(result, "x-wav");
    }
    else if (strstr(".weba", filetype)!=NULL){
      strcat(result, "webm");
    }
    else {
      strcat(result, filetype);
    }
  }
  free(filetype);
  filetype = 0;
  return result;
}

