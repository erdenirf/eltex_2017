#include <stdio.h>
#include <stdlib.h>
#include "dstruct.h"

int main (int argc, char *argv[]) {
	char line[MAXLEN];
	int N = 0;		//size of array of struct ELEM
	printf("Enter array's size of struct ELEM = ");
	fgets(line, MAXLEN, stdin);
	sscanf(line, "%d", &N);
	struct ELEM *massiv = NULL;	//link to array of struct ELEM
	massiv = (struct ELEM *)malloc(N*sizeof(struct ELEM));
	for (int i=0; i<N; ++i) {
		input_struct(&massiv[i]);
	}
	int (*func)(const void *, const void *);
	func = cmpfunc;
	sort_struct_array(massiv, N, func);
	printf("\tOutput data:\n");
	for (int i=0; i<N; ++i) {
		output_struct(&massiv[i]);
	}
	free (massiv);
	massiv = 0;
	return 0;
}

/*
* Sort array of struct ELEM
*/
void sort_struct_array(struct ELEM *array, int sizearray, int (*cmp)(const void *, const void *)) {
	for (int i=0; i<sizearray-1; ++i) {
		for (int j=i+1; j<sizearray; ++j) {
			if (cmp(array+i, array+j)) {
				swap(array+i, array+j);
			}
		}
	}
}

/**
* Compare function, to send in parameter of sort
**/
int cmpfunc(const void *p1, const void *p2)
{
	struct ELEM * pp1 = (struct ELEM *)p1;
	struct ELEM * pp2 = (struct ELEM *)p2;
	return (pp1->date.mm > pp2->date.mm);
}

/*
* Swap memory address of two variables
*/
void swap(struct ELEM *a, struct ELEM *b) {
	struct ELEM temp = *a;
	*a = *b;
	*b = temp;
}

/*
* Input struct from stdin
*/
void input_struct(struct ELEM *item) {
		char line[MAXLEN];
		printf("Enter name = ");
		fgets(item->name, MAXLEN, stdin);
		printf("Enter date (##.##.####) = ");
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%d.%d.%d", &item->date.dd, &item->date.mm, &item->date.year);
		printf("Enter price = ");
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%f", &item->price);
		printf("Enter count = ");
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%d", &item->count);
}

/*
* Output struct to stdout
*/
void output_struct(struct ELEM *item) {
	printf("name = %s\n", item->name);
	printf("date = %d.%d.%d\n", item->date.dd, item->date.mm, item->date.year);
	printf("price = %f\n", item->price);
	printf("count = %d\n", item->count);
}
