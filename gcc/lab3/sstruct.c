#include <stdio.h>
#define MAXSIZE 2
#define MAXLEN 100

struct DATE {
	int year;
	int dd;
	int mm;
};

struct ELEM {
	char name[MAXLEN];
	struct DATE date;
	float price;
	int count;
};

int main (int argc, char *argv[]) {
	struct ELEM massiv[MAXSIZE];
	char line[MAXLEN];
	for (int i=0; i<MAXSIZE; ++i) {
		printf("name[%d] = ", i);
		fgets(massiv[i].name, MAXLEN, stdin);
		printf("date[%d] (##.##.####) = ", i);
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%d.%d.%d", &massiv[i].date.dd, &massiv[i].date.mm, &massiv[i].date.year);
		printf("price[%d] = ", i);
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%f", &massiv[i].price);
		printf("count[%d] = ", i);
		fgets(line, MAXLEN, stdin);
		sscanf(line, "%d\n", &massiv[i].count);
	}
	printf("\tOutput data:\n");
	for (int i=0; i<MAXSIZE; ++i) {
		printf("name[%d] = %s\n", i, massiv[i].name);
		printf("date[%d] = %d.%d.%d\n", i, massiv[i].date.dd, massiv[i].date.mm, massiv[i].date.year);
		printf("price[%d] = %f\n", i, massiv[i].price);
		printf("count[%d] = %d\n", i, massiv[i].count);
	}
	return 0;
}
