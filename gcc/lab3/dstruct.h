#ifndef DSTRUCT
#define DSTRUCT

#define MAXLEN 100

struct DATE {
	int year;
	int dd;
	int mm;
};

struct ELEM {
	char name[MAXLEN];
	struct DATE date;
	float price;
	int count;
};

void input_struct(struct ELEM *item);
void output_struct(struct ELEM *item);
int cmpfunc(const void *p1, const void *p2);
void sort_struct_array(struct ELEM *array, int sizearray, int (*cmp)(const void *, const void *));
void swap(struct ELEM *a, struct ELEM *b);

#endif
