#ifndef AVIA_H
#define AVIA_H
#include <stdio.h>

struct soldier {
  int x;        // i
  int y;        // j
  char name;    // '0'..'9'
  int dx;       // di направление движения по вертикали
  int dy;       // dj направление движения по горизонтали
  int score;    // count of eat TARGET
};
typedef struct soldier Soldier;

struct group {
  int matrix_rows;    //количество строк
  int matrix_columns;   //фиксированная длина строк
  char * matrix;        //массив строк
  int soldiers_count;   //количество солдатов
  Soldier * soldiers_list;    //массив солдатов
};

enum sym_soldier {SOLDIER0 = '0', SOLDIER1, SOLDIER2, SOLDIER3, SOLDIER4, SOLDIER5, SOLDIER6, SOLDIER7, SOLDIER8, SOLDIER9}; //перечисляемые символы, обозначающий солдата на карте
enum sym_target {TARGET='*'}; //символ, обозначающий цель
enum sym_space {SPACE='-'}; //символ, обозначающий свободную дорожку

size_t sizeof_struct_group(struct group * psgroup);
void * pack_struct_group(struct group * psgroup);
struct group * unpack_struct_group(void * link);
Soldier soldier_init(char * matrix, int matrix_rows, int matrix_columns, int row, int column);
int is_in_borders(int matrix_rows, int matrix_columns, int row, int column);
int get_count_active_soldiers(Soldier * soldiers_list, int count, char * matrix, int matrix_rows, int matrix_columns);
void soldier_move(Soldier * soldier, char * matrix, int matrix_row, int matrix_column);
Soldier * get_array_soldiers(char * matrix, int matrix_rows, int matrix_columns, int * count);
void fprintln_soldier(FILE * stream, Soldier * one);
int finp_str(FILE * stream, char* string, int maxlen);
void fout_str(FILE * stream, char* string, int length, int number);
char* malloc_array_string(int matrix_rows, int matrix_columns);
void ClearScreen();   //hack to clear

#endif
