#include "fork_shm.h"
#include "posix_shm.c"
#include "sem.c"
#define BUFSIZE 255
#define PAGESIZE 2048

/*
 * Создать 1 дочерний процесс
 */
pid_t do_fork(int num) {
  pid_t pid;
  pid = fork();
  if (-1 == pid) {
    perror("fork"); /* произошла ошибка */
    exit(1); /*выход из родительского процесса*/
  }
  else if (0 == pid){
    printf(" CHILD: Это процесс-потомок! Мой PID=%d, У родителя PPID=%d\n", getpid(), getppid());
    fflush(stdout);
    char str[BUFSIZE];
    strcpy(str, "");
    snprintf(str, BUFSIZE, "%d", num);
    execl("./child", "./child", str, NULL);
    _exit(EXIT_SUCCESS); /* выход из процесс-потомока */ 
  }
  return pid;
}

/*
 * Ожидание 1 дочернего процесса
 */
int wait_fork(pid_t pid) {
  int stat;
  if (0 < pid) {
    int waitstatus = waitpid(pid, &stat, 0);
    if (pid == waitstatus)
      {
	  printf(" PARENT: процесс-потомок %d done,  exitcode=%d, мой pid=%d\n", pid, WEXITSTATUS(stat), getpid());
	  fflush(stdout);
	  key_t kluch = getkey("Erdeni", 0);
	  int sem = getsemid(kluch);
	  semlock(sem, 1);
	  /* Разделяемая память начало */

	  int shmid = shm_open_readwrite_create("/Erdeni_Bairovich", 0777);
  size_t bytes = PAGESIZE;
  void * ptr = mmap_memory(bytes, shmid, 0);
  struct group * rasp = NULL;
  rasp = unpack_struct_group(ptr);
  munmap(ptr, bytes);
  close(shmid);

	  /* Разделяемая память конец */
	  semunlock(sem, 1);
	   ClearScreen();
  printf("%d\n", rasp->soldiers_count);
  for(int i=0; i!=rasp->soldiers_count; ++i) {
    fprintln_soldier(stdout, rasp->soldiers_list+i);
  }
  printf("%d\n", rasp->matrix_rows);
  printf("%d\n", rasp->matrix_columns);
  fout_str(stdout, rasp->matrix, strlen(rasp->matrix), strlen(rasp->matrix));
        }
    if (pid != waitstatus)
      {
      kill(pid,SIGKILL);
      printf(" PARENT: Thread #%d killed!\n",pid);
      fflush(stdout);
    }
  }
  return WEXITSTATUS(stat);
}
