#ifndef FORK_SHM_H
#define FORK_SHM_H

#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

pid_t do_fork(int num);  /* Создать 1 дочерний процесс */
int wait_fork(pid_t pid); /* Ожидание 1 дочернего процесса */

#endif
