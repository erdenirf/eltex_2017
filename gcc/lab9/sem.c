#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <ctype.h>
#include "sem.h"

/*
 * Token get key from char * and int
 */
key_t getkey(const char * name, int id) {
  return ftok(name, id);
}

/*
 * Get semaphore id from token key
 */
int getsemid(key_t key) {
  return semget(key, 1, 0666 | IPC_CREAT);
}

/*
 * Set value to semaphore
 */
void semset(int semid, int num, int value) {
  union semun arg;
  arg.val = value;
  semctl(semid, num, SETVAL, arg);
}

/*
 * Lock semaphore (dec value)
 */
int semlock(int semid, int value) {
  return semop(semid, &lock_res, value);
}

/*
 * Unlock semaphore (inc value)
 */
int semunlock(int semid, int value) {
  return semop(semid, &rel_res, value);
}
