#include "avia.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * size of struct group *
 */
size_t sizeof_struct_group(struct group * psgroup) {
  size_t bytes = 1;
  bytes+=sizeof(psgroup->matrix_rows)+sizeof(psgroup->matrix_columns);
  bytes+=sizeof(char)*strlen(psgroup->matrix);
  bytes+=sizeof(psgroup->soldiers_count);
  bytes+=sizeof(Soldier)*psgroup->soldiers_count;
  return bytes;
}

/*
 * Pack struct group * to void *
 */
void * pack_struct_group(struct group * psgroup) {
  void * pointer = NULL;
  size_t bytes = sizeof_struct_group(psgroup);
  pointer = malloc(bytes);
  size_t offset = 0;
  memmove(pointer+offset, &(psgroup->soldiers_count), sizeof(int));
  offset += sizeof(int);
  memmove(pointer+offset, (void *)psgroup->soldiers_list, sizeof(Soldier)*psgroup->soldiers_count);
  offset += sizeof(Soldier)*psgroup->soldiers_count;
  memmove(pointer+offset, &(psgroup->matrix_rows), sizeof(int));
  offset += sizeof(int);
  memmove(pointer+offset, &(psgroup->matrix_columns), sizeof(int));
  offset += sizeof(int);
  memmove(pointer+offset, (void *)psgroup->matrix, sizeof(char)*psgroup->matrix_rows*psgroup->matrix_columns);
  return pointer;
}

/*
 * Unpack void * to struct group *
 */
struct group * unpack_struct_group(void * pointer) {
  struct group * ret = NULL;
  ret = (struct group *)malloc(sizeof(struct group));  // 0+
  void * buf_int = NULL;
  buf_int = malloc(sizeof(int));                // 1+
  size_t off = 0;
  memmove(buf_int, pointer+off, sizeof(int));
  int * count = (int *)buf_int;
  ret->soldiers_count = *count;
  void * buf_list = NULL;
  buf_list = malloc(sizeof(Soldier)*(*count));       // 2+
  off += sizeof(int);
  memmove(buf_list, pointer+off, sizeof(Soldier)*(*count));
  ret->soldiers_list = (Soldier *)buf_list;
  off += sizeof(Soldier)*(*count);
  memmove(buf_int, pointer+off, sizeof(int));
  int * addr_row = (int *)buf_int;
  ret->matrix_rows = *addr_row;
  off += sizeof(int);
  memmove(buf_int, pointer+off, sizeof(int));
  int * addr_columns = (int *)buf_int;
  ret->matrix_columns = *addr_columns;
  void * string = NULL;
  string = malloc((*addr_row)*(*addr_columns)*sizeof(char));    // 3+
  off += sizeof(int);
  memmove(string, pointer+off, sizeof(char)*(*addr_row)*(*addr_columns));
  ret->matrix = (char *)string;
  free(buf_int);                       // 1-
  buf_int = NULL;
  return ret;
}

/*
 * Dirty hack to clear screen
 */
void ClearScreen() {
  printf( "\033[2J" );
  fflush(stdout);
}

/*
 * is in borders
 */
int is_in_borders(int matrix_rows, int matrix_columns, int row, int column) {
  return (row>=0 && column>=0 && row<matrix_rows && column<matrix_columns-1);
}

/**
 * first init of one soldier
 */
Soldier soldier_init(char * matrix, int matrix_rows, int matrix_columns, int row, int column) {
  if (!is_in_borders(matrix_rows, matrix_columns, row, column))  {
    fprintf(stderr, "Soldier вышел за границы матрицы");
    fflush(stderr);
    exit(-1);  //вернуть ошибку
  }
  Soldier ret;
  ret.x = column;
  ret.y = row;
  ret.name = matrix[row*matrix_columns+column];
  ret.score = 0;
  int maxrow = (row >= matrix_rows-row) ? row : matrix_rows-row;
  int maxcolumn = (column >= matrix_columns-column) ? column : matrix_columns-column;
  if (maxrow > maxcolumn) {
    ret.dx = 0;
    if (maxrow == row) {
      ret.dy = -1;
    }
    else {
      ret.dy = 1;
    }
  }
  else {
    ret.dy = 0;
    if (maxcolumn == column) {
      ret.dx = -1;
    }
    else {
      ret.dx = 1;
    }
  }
  return ret;
}

/**
 * Soldier move in matrix and eat TARGET
 */
void soldier_move(Soldier * soldier, char * matrix, int matrix_row, int matrix_column) {
  if (!is_in_borders(matrix_row, matrix_column, soldier->y, soldier->x)){
    return;   //вышли за границы матрицы
  }
  matrix[soldier->y * matrix_column + soldier->x] = SPACE;
  soldier->y += soldier->dy;
  soldier->x += soldier->dx;
  if (!is_in_borders(matrix_row, matrix_column, soldier->y, soldier->x))
  {
    return;   //вышли за границы матрицы
  }
  if (matrix[soldier->y * matrix_column + soldier->x] == TARGET) {
    ++soldier->score;
  }
  matrix[soldier->y * matrix_column + soldier->x] = soldier->name;
}

/**
 * Get list of soldiers from matrix
 */
Soldier * get_array_soldiers(char * matrix, int matrix_rows, int matrix_columns, int * count) {
  Soldier * ret = NULL;
  size_t retsize = 0;
  for (int i=0; i!=matrix_rows*matrix_columns; ++i) {
    if (matrix[i] >= SOLDIER0 && matrix[i] <= SOLDIER9) {
      ++retsize;
      ret = realloc(ret, retsize*sizeof(Soldier));
      ret[retsize-1] = soldier_init(matrix, matrix_rows, matrix_columns, i/matrix_columns, i%matrix_columns);
    }
  }
  (*count) = retsize;
  return ret;
}

/*
 * Count of active soldiers in list 
 */
int get_count_active_soldiers(Soldier * soldiers_list, int count, char * matrix, int matrix_rows, int matrix_columns){
  int ret=0;
  for (int i=0; i!=count; ++i) {
    if (is_in_borders(matrix_rows, matrix_columns, soldiers_list[i].y, soldiers_list[i].x)) {
      ++ret;
    }
  }
  return ret;
}

/**
 * println one soldier
 */
void fprintln_soldier(FILE * stream, Soldier * one) {
  fprintf(stream, "'%c': y=%d, x=%d, dy=%d, dx=%d, score=%d\n", one->name, one->y, one->x, one->dy, one->dx, one->score);
  fflush(stream);
}

/**
* readln string with input maximum length
**/
int finp_str(FILE * stream, char* string, int maxlen) {
	fgets(string, maxlen, stream);
	return strlen(string);
}

/**
* println first number chars of string
**/
void fout_str(FILE * stream, char* string, int length, int number) {
	if (number > length) {
		number = length;
	}
	for (int i=0; i<number; ++i) {
	  fprintf(stream, "%c", *(string+i));
	}
	fflush(stream);
}


/**
* malloc memory for array of string
**/
char* malloc_array_string(int matrix_rows, int matrix_columns) {
	char* ret = NULL;
	ret = (char*)malloc(matrix_rows*matrix_columns*sizeof(char));
	return ret;
}


