#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "posix_shm.h"

/**
 * POSIX shared open rdonly
 */
int shm_open_readonly(const char * name) {
  return shm_open(name, O_RDONLY, 0777);
}

/**
 * POSIX shared open rdonly && create
 */
int shm_open_readonly_create(const char * name, mode_t mode) {
  return shm_open(name, O_RDONLY | O_CREAT, mode);
}

/**
 * POSIX shared open rdonly && create && excl
 */
int shm_open_readonly_create_excl(const char * name, mode_t mode) {
  return shm_open(name, O_RDONLY | O_CREAT | O_EXCL, mode);
}

/**
 * POSIX shared open rw
 */
int shm_open_readwrite(const char * name) {
  return shm_open(name, O_RDWR, 0777);
}

/**
 * POSIX shared open rw && create
 */
int shm_open_readwrite_create(const char * name, mode_t mode) {
  int shm = shm_open(name, O_RDWR | O_CREAT, mode);
  if (shm == -1) {
    perror("shm_open");
    exit(-1);
  }
  return shm;
}

/**
 * POSIX shared open rw && create && excl
 */
int shm_open_readwrite_create_excl(const char * name, mode_t mode) {
  return shm_open(name, O_RDWR | O_CREAT | O_EXCL, mode);
}

/**
 * POSIX set size of memory in bytes
 */
int ftruncate_memory(int shmid, size_t bytes) {
  int ftrunc = ftruncate(shmid, bytes+1);
  if (ftrunc == -1) {
    perror("ftruncate");
    exit(-1);
  }
  return ftrunc;
}

/**
 * POSIX shared attach memory to own proccess virtual memory
 */
void * mmap_memory(size_t bytes, int shmid, off_t offset) {
  void * addr = mmap(NULL, bytes+1, PROT_WRITE | PROT_READ, MAP_SHARED, shmid, offset);
    if (addr == (void*)-1 ) {
    perror("mmap");
    exit(-1);
    }
  return addr;
}

/**
 * POSIX shared memcpy
 */
void mem_write(void * dest, size_t destSize, void * src, size_t srcSize) {
  size_t len = (destSize>=srcSize) ? srcSize : destSize;
  memcpy(dest, src, len);
}

/**
 * POSIX shared memory unmap (detach)
 */
void munmap_memory(void * addr, size_t bytes) {
  munmap(addr, bytes);
}

