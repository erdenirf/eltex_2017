#include "avia.c"
#include "posix_shm.c"
#include <stdio.h>
#include "sem.c"

#define PAGESIZE 2048

int main(int argc, char * argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Error. Usage: %s <text>\n", argv[0]);
    exit(-1);
    }
  int num = atoi(argv[1]);
  key_t kluch = getkey("Erdeni", 0);
  int sem = getsemid(kluch);
  semlock(sem, 1);
  /* Разделяемая память начало */
  int shmid = shm_open_readwrite_create("/Erdeni_Bairovich", 0777);
  size_t bytes = PAGESIZE;
  void * ptr = mmap_memory(bytes, shmid, 0);
  struct group * rasp = NULL;
  rasp = unpack_struct_group(ptr);
  if (is_in_borders(rasp->matrix_rows, rasp->matrix_columns, rasp->soldiers_list[num].y, rasp->soldiers_list[num].x)) {
    soldier_move(rasp->soldiers_list+num, rasp->matrix, rasp->matrix_rows, rasp->matrix_columns);
    void * object = pack_struct_group(rasp);
    memcpy(ptr, object, sizeof_struct_group(rasp));
  }
  else {
    munmap_memory(ptr, bytes);
    close(shmid);
    semunlock(sem, 1);
    exit(-1);
  }
  munmap_memory(ptr, bytes);
  close(shmid);
  /* Разделяемая память конец */
  semunlock(sem, 1);
  sleep(1);
  return 0;
}
