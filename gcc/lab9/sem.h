#ifndef SEM_H
#define SEM_H

union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

key_t getkey(const char * name, int id);
int getsemid(key_t key);
void semset(int semid, int num, int value);
int semlock(int semid, int value);
int semunlock(int semid, int value);


#endif
