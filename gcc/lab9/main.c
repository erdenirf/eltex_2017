#include "avia.c"
#include "fork_shm.c"
#include <unistd.h>
#include <string.h>
#include <sys/sem.h>

#define filename "in.txt"

int main(int argc, char * argv[]) {
  struct group * buffer = NULL;
  buffer = (struct group *)malloc(sizeof(*buffer));
  buffer->matrix_rows=0;
  buffer->matrix_columns=0;
  buffer->matrix=NULL;
  buffer->soldiers_count=0;
  buffer->soldiers_list=NULL;
  FILE * in = fopen(filename, "r");
  if (in == NULL) {
    fprintf(stderr, "%s not open", filename);
    exit(-1);
  }
  fscanf(in, "%d%d\n", &buffer->matrix_rows, &buffer->matrix_columns);
  buffer->matrix = malloc_array_string(buffer->matrix_rows,buffer->matrix_columns);
  strcpy(buffer->matrix, "");
  for (int i=0; i!=buffer->matrix_rows; ++i) {
    const int SIZEBUF = 255;
    char temp[SIZEBUF];
    finp_str(in, temp, SIZEBUF);
    strncat(buffer->matrix, temp, buffer->matrix_columns);
  }
  fclose(in);
  buffer->soldiers_list = get_array_soldiers(buffer->matrix, buffer->matrix_rows, buffer->matrix_columns, &(buffer->soldiers_count));
  
  /* Разделяемая память начало */
  
  int shmid = shm_open_readwrite_create("/Erdeni_Bairovich", 0777);
  size_t bytes = sizeof_struct_group(buffer);
  printf("%u\n", bytes);
  ftruncate_memory(shmid, bytes);
  void * ptr = mmap_memory(bytes, shmid, 0);
  void * pointer = NULL;
  pointer = pack_struct_group(buffer);
  memcpy(ptr, pointer, bytes);
  munmap_memory(ptr, bytes);
  close(shmid);
  /* Разделяемая память конец */

  key_t kluchik = getkey("Erdeni", 0);
  int sema = getsemid(kluchik);
  semset(sema, 0, 1);
  
  /* fork() начало */
  pid_t pid[buffer->soldiers_count];
  int exitcode[buffer->soldiers_count];
  for (int i=0; i!=buffer->soldiers_count; ++i) {
    pid[i] = do_fork(i);
  }
  int success_count = 0;
  for (int i=0; i!=buffer->soldiers_count; ++i) {
    exitcode[i] = wait_fork(pid[i]);
    if (exitcode[i] == 0) {
      ++success_count;
    }
  }
  printf("success %d\n", success_count);

  while (success_count>0) {
    for (int i=0; i!=buffer->soldiers_count; ++i) {
      if (exitcode[i] == 0) {
	pid[i] = do_fork(i);
      }
    }
    for (int i=0; i!=buffer->soldiers_count; ++i) {
      if (exitcode[i] == 0) {
	exitcode[i] = wait_fork(pid[i]);
      }
      if (exitcode[i] != 0) {
	--success_count;
      }
    }
  }
  
  /* fork() конец */
  
  free(buffer->matrix);
  free(buffer->soldiers_list);
  free(buffer);  
   return 0;
}
