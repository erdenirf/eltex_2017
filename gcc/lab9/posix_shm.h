#ifndef POSIX_SHM_H
#define POSIX_SHM_H

int shm_open_readonly(const char * name);
int shm_open_readonly_create(const char * name, mode_t mode);
int shm_open_readonly_create_excl(const char * name, mode_t mode);
int shm_open_readwrite(const char * name);
int shm_open_readwrite_create(const char * name, mode_t mode);
int shm_open_readwrite_create_excl(const char * name, mode_t mode);
int ftruncate_memory(int shmid, size_t bytes);
void * mmap_memory(size_t bytes, int shmid, off_t offset);
void mem_write(void * dest, size_t destSize, void * src, size_t srcSize);
void munmap_memory(void * addr, size_t bytes);

#endif
