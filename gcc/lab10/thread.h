#ifndef THREAD_H
#define THREAD_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

pthread_t create_thread(void * (*func) (void *), void * arg);
void * join_thread(pthread_t thread);
void detach_thread(pthread_t thread);

#endif
