#include "thread.h"

/*
 * Create thread
 */
pthread_t create_thread(void * (*func) (void *), void * arg) {
  pthread_t result = 0;
  int code = pthread_create(&result, NULL, func, arg);
  if (code != 0) {
    perror("pthread_create");
    exit (EXIT_FAILURE);
  }
  return result;
}

/*
 * Wait thread
 */
void * join_thread(pthread_t thread) {
  void * result = NULL;
  int code = pthread_join(thread, &result);
  if (code != 0) {
    perror("pthread_join");
    exit (EXIT_FAILURE);
  }
  return result;
}

/*
 * Detach thread
 */
void detach_thread(pthread_t thread) {
  pthread_detach(thread);
}
