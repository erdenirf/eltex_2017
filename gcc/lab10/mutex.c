#include "mutex.h"

/**
 * Initialization mutex
 */
void init_mutex(pthread_mutex_t * mutex) {
  pthread_mutex_init(mutex, NULL);
}

/**
 * Delete mutex
 */
void destroy_mutex(pthread_mutex_t * mutex) {
  pthread_mutex_destroy(mutex);
}

/**
 * Wait and lock mutex
 */
void wait_lock_mutex(pthread_mutex_t * mutex) {
  pthread_mutex_lock(mutex);
}

/**
 * Unlock locked mutex
 */
void unlock_mutex(pthread_mutex_t * mutex) {
  pthread_mutex_unlock(mutex);
}
