#ifndef MUTEX_H
#define MUTEX_H

void init_mutex(pthread_mutex_t * mutex);
void destroy_mutex(pthread_mutex_t * mutex);
void wait_lock_mutex(pthread_mutex_t * mutex);
void unlock_mutex(pthread_mutex_t * mutex);

#endif
