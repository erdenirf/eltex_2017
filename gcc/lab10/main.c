#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include "thread.c"
#include <string.h>
#include "mutex.c"
#define INPUTFILE "in.txt"
#define OUTPUTFILE "out.txt"
#define ARRAYSIZE 30

pthread_mutex_t * mymutex = NULL;

/**
 * Fopen, Fprintln, Fclose
 */
int fopen_fprintln_fclose(const char * name, int a, int b, int * array, int array_size) {
  FILE * out = NULL;
  out = fopen(name, "at");
  if (out == NULL) {
    perror("fopen");
    exit(-1);
  }
  fprintf(out, "%d %d\n", a, b);
  for (int i=0; i!=array_size; ++i) {
    fprintf(out, "%d ", array[i]);
  }
  fprintf(out, "\n");
  fflush(out);
  fclose(out);
  return 0;
}

/**
 * Calculate simple number from a to b
 */
int * calc_simple_numbers(int a, int b, int * array_size) {
  int * array = NULL;
  *array_size = 0;
  for (int i=a; i!=b; ++i) {
    int simple = 1;
    for (int j=2; j!=(int)round(sqrt(1.0*i)); ++j) {
      if (i%j==0) {
	simple = 0;
      }
    }
    if (simple != 0) {
      ++(*array_size);
      array = realloc(array, (*array_size)*sizeof(int));
      array[(*array_size)-1] = i;
    }
  }
  return array;
}

/**
 * Thread func arg
 */
struct diapason {
  int A;
  int B;
};

/**
 * Thread func
 */
void *func(void *arg){
  struct diapason * inarg = NULL;
  inarg = (struct diapason *)arg;
  int * massiv = NULL;
  int msize = 0;
  massiv = calc_simple_numbers(inarg->A, inarg->B, &msize);
  wait_lock_mutex(mymutex);     // MUTEX wait and lock
  fopen_fprintln_fclose(OUTPUTFILE, inarg->A, inarg->B, massiv, msize);
  unlock_mutex(mymutex);        // MUTEX unlock
  printf("pthread id=%lu \n", pthread_self());
  return NULL;
}

int main(int argc, char * argv[]){
  int N  = 0;
  struct diapason Arrays[ARRAYSIZE];
  FILE * in = fopen(INPUTFILE, "r");
  if (in == NULL) {
    perror("fopen");
    exit(-1);
  }
  fscanf(in, "%d", &N);
  for (int i=0; i!=N; ++i) {
    fscanf(in, "%d%d", &(Arrays[i].A), &(Arrays[i].B));
  }
  fclose(in);
  FILE * out = NULL;
  out = fopen(OUTPUTFILE, "wt"); //(открываем)создаем файл на запись
  if (out == NULL) {
    perror("fopen");
    exit(-1);
  }
  fclose(out);  //закрываем файл
  mymutex = (pthread_mutex_t *)malloc(sizeof(*mymutex));
  init_mutex(mymutex);          // MUTEX init
  pthread_t threads[N];	
  for (int i = 0; i < N; i++){
    threads[i] = create_thread(func, &(Arrays[i]));  //создать поток
    pthread_detach(threads[i]);		//Отсоединить поток
  }
  sleep(1);
  printf("Done..\n");
  destroy_mutex(mymutex);    // MUTEX destructor
  free(mymutex);
  return 0;
}
