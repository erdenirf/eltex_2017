#ifndef LAB2_H
#define LAB2_H

int inp_str(char* string, int maxlen);
void out_str(char* string, int length, int number);
char** malloc_array_string(int arraysize, int maxlen);
void free_array_string(char** array, int arraysize);
int str_len_cmp(const char *str1, const char *str2);
int str_alpha_cmp(const char *str1, const char *str2);
void sort(char** ar, int arraysize, int (*cmpbool)(const void *, const void *));
int cmpstringp(const void *p1, const void *p2);

#endif
