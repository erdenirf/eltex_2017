#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lab2.h"

int main(int argc, char *argv[]) {
	const int MAXLEN = 255;
	printf("\tEnter Count of Strings: ");
	int N = 0;
	scanf("%d\n", &N);
	char **matrix = NULL;		//link to 0 string
	matrix = malloc_array_string(N, MAXLEN);
	for (int i=0; i<N; i++) {
		int len=inp_str(*(matrix+i), MAXLEN);
		printf("\tlength[%d] = %d\n", i, len);
	}
	int (*func)(const void *, const void *);
	func = cmpstringp;
	sort(matrix, N, func);
	printf("\tsorted result:\n");
	for (int i=0;i<N;++i){
		out_str(matrix[i],strlen(matrix[i]),strlen(matrix[i]));
	}
	free_array_string(matrix, N);
	return 0;
}

/**
* Sort one array of C-string
**/
void sort(char** ar, int arraysize, int (*cmpbool)(const void *, const void *)) {
	for (int i=0; i<arraysize-1; ++i) {
		for (int j=i+1; j<arraysize; ++j) {
			if ((*cmpbool)(ar+i, ar+j)) {
				char* temp_address;	//link to 0 char of string
				temp_address = ar[i];
				ar[i]=ar[j];
				ar[j]=temp_address;
			}
		}
	}

}

/**
* Compare two strings [char**] and [char**]
**/
int cmpstringp(const void *p1, const void *p2) {
	char **pp1 = (char**)p1;
	char **pp2 = (char**)p2;
	return (str_alpha_cmp(*pp1, *pp2) > 0);
}

/**
* Compare two C-strings with alphabet
**/
int str_alpha_cmp(const char *str1, const char *str2) {
	int len1 = strlen(str1);
	int len2 = strlen(str2);
	int len3 = (len1>len2) ? len1 : len2;
	for (int i=0; i<len3; ++i) {
		if (str1[i] < str2[i]) {
			return -1;
		}
		else if (str1[i] > str2[i]) {
			return 1;
		}
	}
	return 0;
}

/**
* Compare length of two C-strings
**/
int str_len_cmp(const char *str1, const char *str2) {
	if (strlen(str1) < strlen(str2)){
		return -1;
	}
	else if (strlen(str1) > strlen(str2)){
		return 1;
	}
	return 0;
}

/**
* readln string with input maximum length
**/
int inp_str(char* string, int maxlen) {
	fgets(string, maxlen, stdin);
	int len = strlen(string) - 1;
	string[len] = '\0';
	return len;
}

/**
* println first number chars of string
**/
void out_str(char* string, int length, int number) {
	if (number > length) {
		number = length;
	}
	for (int i=0; i<number; ++i) {
		printf("%c", *(string+i));
	}
	printf("\n");
}

/**
* malloc memory for array of string
**/
char** malloc_array_string(int arraysize, int maxlen) {
	char** mas;
	mas = (char**)malloc(arraysize*sizeof(char*));
	for (int i=0; i<arraysize; ++i) {
		mas[i] = (char*)malloc(maxlen*sizeof(char));
	}
	return mas;
}

/**
* free memory array of string
**/
void free_array_string(char** array, int arraysize) {
	for (int i=0; i<arraysize; i++) {
		free (array[i]);
		array[i] = 0;
	}
	free(array);
	array = 0;
}
