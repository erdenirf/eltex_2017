#include "msg.h"

/*
 * Инициализация очереди сообщений
 */
int msg_init(const char *pathname, int proj_id) {
  key_t key = ftok(pathname, proj_id);
  int msgqid=0;
  if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
    perror("msgget");
    exit(1);
  }
  return msgqid;
}

/*
 * Отправить сообщений в очередь сообщений
 */
void send_message(int qid, struct mymsgbuf *qbuf, long type, char *text){
        qbuf->mtype = type;
        strcpy(qbuf->mtext, text);

        if((msgsnd(qid, (struct msgbuf *)qbuf,
                strlen(qbuf->mtext)+1, 0)) ==-1){
                perror("msgsnd");
                exit(1);
        }
}

/*
 * Считать сообщение из очереди сообщений
 */
void read_message(int qid, struct mymsgbuf *qbuf, long type){
        qbuf->mtype = type;
        msgrcv(qid, (struct msgbuf *)qbuf, MAX_SEND_SIZE, type, 0);
        printf("Type: %ld Text: %s\n", qbuf->mtype, qbuf->mtext);
}
