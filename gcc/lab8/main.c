#include "crc.c"
#include "fork_msg.c"

int main(int argc, char * argv[]) {
  if (argc < 2) {
        printf("Usage: ./fork_many text text ...\n");
        exit(-1);
    }
  int message_id=msg_init(argv[0], 'e');
  int message_qtype=1;
  int pid[argc];
  for (int i = 1; i < argc; i++) {
    // запускаем дочерний процесс
    char buf[MAX_SEND_SIZE];
    snprintf(buf, MAX_SEND_SIZE, "%u CRC %s", fcrc_32(argv[i]), argv[i]);
    pid[i]=do_fork(message_id, message_qtype, buf);
  }
  // если выполняется родительский процесс
    printf("PARENT: Это процесс-родитель!\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (int i = 1; i < argc; i++) {
      char * ret = NULL;   //буфер для чтения сообщения
      ret=wait_fork(pid[i], message_id, message_qtype);
    }
    // проверка в конце
    int rc;
    if ((rc = msgctl(message_id, IPC_RMID, NULL)) < 0) {
		perror( strerror(errno) );
		printf("msgctl (return queue) failed, rc=%d\n", rc);
		return 1;
	}
  return 0;
}
