#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include "crc.h"

/*
 * Открыть файл. Посчитать контрольную сумму CRC.
 * (Вернуть сумму кодов всех символов). Закрыть файл.
 */
uint32_t fcrc_32(const char * filename) {
  uint32_t sum = 0;
  int readfd = open(filename, O_RDONLY, 0);
  if (readfd < 0) {
    fprintf(stderr, "%s not open for read", filename);
    exit(-1);
  }
  char buf;
  uint32_t len;
  while ( (len=read(readfd, &buf, sizeof(buf))) > 0) {
    sum += (uint8_t)buf;
  }
  close(readfd);
  sum %= UINT32_MAX;   //остаток от деления на беззнаковое макс. число 32 бита
  return sum;
}
