#ifndef FORK_MSG_H
#define FORK_MSG_H

#define BUFSIZE 255

int do_fork(int msgqid, int qtype, char * str); /* Создать 1 наследника и вернуть его pid */
char * wait_fork(int cpid, int msgqid, int qtype); /* Ждать 1 наследника и вернуть сообщение из очереди сообщений */

#endif
