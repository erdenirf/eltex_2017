#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include "msg.c"
#include "fork_msg.h"

/*
 * Создаем 1-го наследника 
*/
int do_fork(int msgqid, int qtype, char * str) {
  struct mymsgbuf qbuf;
        // запускаем дочерний процесс 
        int cpid = fork();
        srand(getpid());
        if (-1 == cpid) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
	else if (0 == cpid) {
	  printf(" CHILD: Это процесс-потомок СТАРТ! PID=%d. У родителя PPID=%d\n", getpid(), getppid());
	  fflush(stdout);
	  send_message(msgqid, (struct mymsgbuf *)&qbuf,
                                       qtype, str); 
	  fprintf(stdout, " CHILD: Это %d процесс-потомок отправил сообщение!\n", getpid());
	fflush(stdout);
            _exit(EXIT_SUCCESS); /* выход из процесс-потомока */
        }
	return cpid;
}

/*
 * Ждем 1-го наследника
 */
char * wait_fork(int cpid, int msgqid, int qtype) {
  char * result = NULL;
  struct mymsgbuf qbuf;
  if (0 < cpid) {
	 int stat;
	 int  status = waitpid(cpid, &stat, 0);
        if (cpid == status) {
	  printf("процесс-потомок %d done,  exitcode=%d, мой pid=%d\n", cpid, WEXITSTATUS(stat), getpid());
	    fflush(stdout);
        }
	read_message(msgqid, &qbuf, qtype);
	result=(char*)malloc(sizeof(char)*(strlen(qbuf.mtext)+1));
	strcpy(result, qbuf.mtext);
	if (cpid != status) {kill(cpid,SIGKILL); printf("Thread #%d killed!\n",cpid);}
	}
  return result;
}
