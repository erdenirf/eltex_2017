#ifndef PIPE_FD_H
#define PIPE_FD_H

void pipe_init(int * pipefd); // Инициализация pipe-канала(неименованный) и выход в случае ошибки
void pipe_write(int * pipefd, char * str); // pipe-канал запись
void pipe_read(int * pipefd, char * buf); //pipe-канал чтение

#endif
