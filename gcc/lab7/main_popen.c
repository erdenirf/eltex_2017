#include <stdio.h>
#include <stdlib.h>

#define input_cmd "./main_fifo"
#define output_cmd "cat"

int main(int argc, char * argv[]) {
  FILE * input = NULL;
  FILE * output = NULL;
  input = popen(input_cmd, "r");
  if (input == NULL) {
    fprintf(stderr, "%s not found", input_cmd);
    exit(-1);
  }
  output = popen(output_cmd, "w");
  if (output == NULL) {
    fprintf(stderr, "%s not found", output_cmd);
    exit(-2);
  }
  char buf;
while (!(feof(input))) {
fscanf(input, "%c", &buf);
fprintf(output, "%c", buf);
}
fflush(output);
  pclose(input);
pclose(output);
  return 0;
}
