#ifndef FIFO_H
#define FIFO_H

int fifo_open(char * fifo_name, char * format); /* Открыть fifo */
void fifo_close(int fifofd); /* Закрыть fifo */
void fifo_init(char * fifo_name); /* Создать файл */
void fifo_write(int writefd, char * str); /* fifo-канал запись */
void fifo_nread(int readfd, char * buf, int length); /* fifo-канал чтение */

#endif
