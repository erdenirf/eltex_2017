#include "pipe_fork.h"
#include "pipefd.c"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>

#define BUFSIZE 255

/*
 * Создаем i-го наследника 
*/
int do_fork(int * pipefd, int i, template_t value) {
  // pipe-канал Инициализация
  pipe_init(pipefd);
        // запускаем дочерний процесс 
        int cpid = fork();
        srand(getpid());
        if (-1 == cpid) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
	else if (0 == cpid) {
	  printf(" CHILD: Это %d процесс-потомок СТАРТ! PID=%d. У родителя PPID=%d\n", i, getpid(), getppid());
	  fflush(stdout);
	  char str[BUFSIZE];
	  snprintf(str, BUFSIZE, template_f, value);
	  pipe_write(pipefd, str);
            _exit(EXIT_SUCCESS); /* выход из процесс-потомока */
        }
	return cpid;
}

/*
 * Ждем i-го наследника
 */
template_t wait_fork(int * pipefd, int cpid, int i) {
  template_t result = 0;
  if (0 < cpid) {
	 int stat;
	 int  status = waitpid(cpid, &stat, 0);
        if (cpid == status) {
            printf("процесс-потомок %d done,  exitcode=%d\n", i, WEXITSTATUS(stat));
	    fflush(stdout);
	    char buf[BUFSIZE];
	    pipe_read(pipefd, buf);
	    result = template_ato(buf);
        }
	if (cpid != status) {kill(cpid,SIGKILL); printf("Thread #%d killed!\n",cpid);}
	}
  return result;
}
