#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include "pipefd.h"

/*
 * Инициализация pipe-канала(неименованный) и выход в случае ошибки
 */
void pipe_init(int * pipefd) {
  int code = pipe(pipefd);
  if (code == -1) {
    fprintf(stderr, "pipe error");
    perror("pipe error");
    exit(EXIT_FAILURE);
  }
}

/*
 * pipe-канал запись
 */
void pipe_write(int * pipefd, char * str) {
  // закрыли канал для чтения
  close(pipefd[0]);
  // записали строку
  write(pipefd[1], str, (strlen(str)+1));
  // закрыли канал для записи
  close(pipefd[1]);
}

/*
 * pipe-канал чтение
 */
void pipe_read(int * pipefd, char * buf) {
  // закрыть pipe-канал для записи
  close(pipefd[1]);
  /* читает из канала 0*/
    read(pipefd[0], buf, sizeof(buf));
    //закрыть канал для чтения
    close(pipefd[0]);
}
