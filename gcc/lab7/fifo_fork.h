#ifndef FIFO_FORK_H
#define FIFO_FORK_H

#define BUFSIZE 255
#define template_t double
#define template_f "%lf"
#define template_ato atof

int do_fork(char * fifo_filename, template_t value); /* Создаем i-го наследника */
template_t wait_fork(char * fifo_filename, int cpid); /* Ждем i-го наследника */

#endif
