#ifndef PIPE_FORK_H
#define PIPE_FORK_H

#define template_t double
#define template_f "%lf"
#define template_ato atof

int do_fork(int * pipefd, int i, template_t value); /* Создаем i-го наследника */
template_t wait_fork(int * pipefd, int cpid, int i); /* Ждем i-го наследника */

#endif
