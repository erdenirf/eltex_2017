#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "fifo.h"

/* 
 * Инициализация fifo-канала (именованного)
 */
void fifo_init(char * fifo_name) {
  // создать именованный канал
  mkfifo(fifo_name, 0666);
}

/*
 * Открыть fifo-канал
 */
int fifo_open(char * fifo_name, char * format) {
  if (strcmp(format, "r") == 0) {
    return open(fifo_name, O_RDONLY, 0);
  }
  if (strcmp(format, "w") == 0) {
    return open(fifo_name, O_WRONLY, 0);
  }
  return -1;
}

/*
 * Закрыть fifo-канал
 */
void fifo_close(int fifofd) {
  close(fifofd);
}

/*
 * fifo-канал запись
 */
void fifo_write(int writefd, char * str) {
  /* ЗАПИСЬ В КАНАЛ IPC */
  write(writefd, str, strlen(str));
}

/*
* fifo-канал чтение
*/
void fifo_nread(int readfd, char * buf, int length) {
  // ВРЕМЕННАЯ СТРОКА
  char temp[length];
  // ОЧИСТИТЬ ВХОДНУЮ СТРОКУ
  strcpy(buf, "");
  // ПОКА ЧИТАЕТСЯ
  int n;
  while ((n = read(readfd, temp, length)) > 0) {
    //добавляем к переменной
    strncat(buf, temp, strlen(temp));
  }
}
