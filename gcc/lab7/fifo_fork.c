#include "fifo_fork.h"
#include "fifo.c"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>

int writefd, readfd;  /* Глобальные переменные файловых дескрипторов для клиент/серверного приложения */

/*
 * Создаем 1-го наследника (server) 
*/
int do_fork(char * fifo_filename, template_t value) {
  // fifo-канал Инициализация
  fifo_init(fifo_filename);
        // запускаем дочерний процесс 
        int cpid = fork();
        srand(getpid());
        if (-1 == cpid) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
	else if (0 == cpid) {
	  char str[BUFSIZE];
	  snprintf(str, BUFSIZE, template_f, value);
	  printf(" CHILD: Это процесс-потомок СТАРТ! PID=%d. У родителя PPID=%d\n", getpid(), getppid());
	  fflush(stdout);
	  /*
	   * Открыть fifo-канал для записи сервером.
	   * Важно! Не закрывать канал.
	   */
	  writefd = fifo_open(fifo_filename, "w");
	  /*
	   * Запись данных в fifo-канал сервером.
	   */
	  fifo_write(writefd, str);
	  exit(EXIT_SUCCESS);   /* Выход из дочернего процесса */
        }
	return cpid;
}

/*
 * Ждем 1-го наследника (client)
 */
template_t wait_fork(char * fifo_filename, int cpid) {
  template_t result = 0;
  if (0 < cpid) {
         char buf[BUFSIZE];
	 /* 
	  * Открываем fifo-канал для чтения клиентом,
	  * В этот момент fifo-канал для чтения должен быть открыт сервером.
	  */
         readfd = fifo_open(fifo_filename, "r");
	 /*
	  * Чтение данных из fifo-канала клиентом.
	  * Только чтение. Не открывает и не закрывает.
	  * Важно! Сделать это до waitpid().
	  */
	 fifo_nread(readfd, buf, BUFSIZE);
	 result = template_ato(buf);
	 int stat;
	 int  status = waitpid(cpid, &stat, 0);
        if (cpid == status) {
	    printf("процесс-потомок %d done, я его родитель %d,  exitcode=%d\n", cpid, getpid(), WEXITSTATUS(stat));
	    fflush(stdout);
	    /*
	     * Закрыть fifo-канал для чтения клиентом.
	     * Важно! Закрывать fifo-каналы должен клиент.
	     */
	    fifo_close(readfd);
	    /*
	     * Закрыть fifo-канал для записи клиентом.
	     * Важно! Закрывать fifo-каналы должен клиент.
	     */
	    fifo_close(writefd);
	    /* Удалить временный файл для fifo-канала */
	    unlink(fifo_filename);
	    
        }
	if (cpid != status) {kill(cpid,SIGKILL); printf("Thread #%d killed!\n",cpid);}
	}
  return result;
}
