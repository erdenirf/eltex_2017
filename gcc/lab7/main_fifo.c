#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "fifo_fork.c"

static struct globalArgs {
  int c;     // [-c flag] Count of workers
  template_t v;     // [-v flag] Speed of get gold by workers
  template_t g;     // [-g flag] Summary amount of gold
} _ARGS;

int main(int argc, char *argv[]) {
  _ARGS.c = 7;
  _ARGS.v = 23.00;
  _ARGS.g = 1000.00;
  int opt=0;
  while ((opt=getopt(argc, argv, "c:v:g:")) != -1) {
    switch(opt) {
    case 'c':
      _ARGS.c=atoi(optarg);   //string to int
      break;
    case 'v':
      _ARGS.v=template_ato(optarg);   //string to double
      break;
    case 'g':
      _ARGS.g=template_ato(optarg);   //string to double
      break;
    default:
      fprintf(stderr, "Cmd args: %s -c <count of units> -v <gold per worker> -g <amount of gold>", argv[0]);
      exit(EXIT_FAILURE);
    }
  }
  
  template_t gold_left = _ARGS.g;
  template_t gold_one = 0;
  pid_t pid[_ARGS.c];    //pids
  char fifo_filenames[_ARGS.c][BUFSIZE];
  // Инициализация имен файлов для fifo-канала
  for (int i=0; i!=_ARGS.c; ++i) {
    snprintf(fifo_filenames[i], BUFSIZE, "/tmp/w%d.fifo", i);
  }
  while (gold_left >= 0)
    {
    // Запускаем пулл наследников
    for (int i=0; i!=_ARGS.c; ++i) {
      if (gold_left >= 0) {
	pid[i] = do_fork(fifo_filenames[i], _ARGS.v);
      }
    }
    // В цикле ждем наследников и по прибытию получаем от них данные
    for (int i=0; i!=_ARGS.c; ++i) {
      gold_one = wait_fork(fifo_filenames[i], pid[i]);
      if (gold_left >= 0) {
	gold_left -= gold_one;
      }
      printf("Gold left = ");
      printf(template_f, gold_left);
      printf("\n");
      fflush(stdout);

    }
  }
  return 0;
}
