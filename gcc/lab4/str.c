#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "str.h"
#define MAXLEN 255

struct globalArgs {
	int x;		// [-x flag]
	int l;		// [-l paramInt]
	char * b;	// [-b paramString]
	char * finp_name;	// [-i paramString]
} * _ARGS;

int main(int argc, char *argv[]){
	int opt=0;
	_ARGS=(struct globalArgs*)malloc(sizeof(struct globalArgs));	//allocate memory for globalArgs
	_ARGS->x=0;
	_ARGS->l=0;
	_ARGS->b=NULL;
	_ARGS->finp_name=NULL;
	printf("use: %s [-i inFileName] [-x] [-b paramString] [-l paramInt]\n", argv[0]);
	while ((opt=getopt(argc, argv, "xi:l:b:")) != -1) {
		switch(opt) {
			case 'i':
				_ARGS->finp_name=malloc_string(MAXLEN);
				snprintf(_ARGS->finp_name, MAXLEN, "%s", optarg);
			break;
			case 'x':
				_ARGS->x=1;
			break;
			case 'l':
				_ARGS->l = atoi(optarg);
			break;
			case 'b':
				_ARGS->b = malloc_string(MAXLEN);
				snprintf(_ARGS->b, MAXLEN, "%s", optarg);	// DONT! use strcpy() -> segment dump
			break;
			default:
				fprintf(stderr, "Error. using: %s [-i inputFile.txt] [-x] [-b paramString] [-l paramInt]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
		printf("-%c %s", opt, optarg);
	}
	FILE *finp = NULL;	//input file stream
	FILE *fout = NULL;	//output file stream
	char *foname = NULL;
	finp=fopen(_ARGS->finp_name, "rt");
	if (finp==NULL) {
		fprintf(stderr, "Error. File <%s> not open\n", _ARGS->finp_name);
		exit(EXIT_FAILURE);
	}
	foname = convert_name_inout(_ARGS->finp_name, MAXLEN);
	fout=fopen(foname, "wt");
	if (fout == NULL) {
		fprintf(stderr, "Error. File <%s> not open for write.\n", foname);
		exit(EXIT_FAILURE);
	}
	char * sline = NULL;
	sline = malloc_string(MAXLEN+1);
	while (!feof(finp)) {
		finp_str(finp, sline, MAXLEN);
		if (_ARGS->l != 0) {		//variant 1
			if ((_ARGS->x==0 && strlen(sline)>_ARGS->l) || (_ARGS->x!=0 && strlen(sline)<=_ARGS->l)) {
				fout_str(fout, sline, strlen(sline), strlen(sline));
			}
			
		}
	 	else if (_ARGS->b != NULL) {	//variant 10
			char * str0 = NULL;
			str0=strstr(sline, _ARGS->b);
			if ((_ARGS->x==0 && str0) || (_ARGS->x!=0 && str0)) {
				fout_str(fout, sline, strlen(sline), strlen(sline));
			}
		}
	}
	free_string(sline);
	fclose(fout);	
	fclose(finp);
	free_string(_ARGS->b);
	free_string(_ARGS->finp_name);
	free_string(foname);
	free(_ARGS);
	_ARGS = NULL;
	return 0;
}

/**
* Convert input filename to output file's name
**/
char* convert_name_inout(const char * str, int maxlen) {
	char * result = NULL;					//return string
	result=(char*)malloc(maxlen*sizeof(char));		//allocate memory
	snprintf(result, maxlen, "%s.out", str);		//copy
	return result;
}

/**
* malloc memory for string
**/
char* malloc_string(int maxlen) {
	return ((char*)malloc(maxlen*sizeof(char)));
}

/**
* free memory of string
**/
void free_string(char* array) {
	free(array);
	array = 0;
}

/**
* readln string from file with input maximum length
**/
int finp_str(FILE * f, char * string, int maxlen) {
	fgets(string, maxlen, f);
	int len = strlen(string) - 1;
	string[len] = '\0';
	return len;
}

/**
* println to file first number chars of string
**/
void fout_str(FILE * f, char * string, int length, int number) {
	if (number > length) {
		number = length;
	}
	for (int i=0; i<number; ++i) {
		fprintf(f, "%c", *(string+i));
	}
	fprintf(f, "\n");
}

/**
* malloc memory for array of string
**/
char** malloc_array_string(int arraysize, int maxlen) {
	char** mas;
	mas = (char**)malloc(arraysize*sizeof(char*));
	for (int i=0; i<arraysize; ++i) {
		mas[i] = (char*)malloc(maxlen*sizeof(char));
	}
	return mas;
}

/**
* free memory array of string
**/
void free_array_string(char** array, int arraysize) {
	for (int i=0; i<arraysize; i++) {
		free (array[i]);
		array[i] = 0;
	}
	free(array);
	array = 0;
}
