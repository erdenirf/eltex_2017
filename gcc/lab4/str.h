#ifndef STRING0
#define STRING0

char* malloc_string(int maxlen);
void free_string(char* array);
int finp_str(FILE *f, char* string, int maxlen);
void fout_str(FILE *f, char* string, int length, int number);
char** malloc_array_string(int arraysize, int maxlen);
void free_array_string(char** array, int arraysize);
char* convert_name_inout(const char * str, int maxlen);

#endif
