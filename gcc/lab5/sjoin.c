#include "sjoin.h"
#include <stdlib.h>
#include <string.h>

/*
 * Соединить строки и поставить между ними указанный разделитель
 */
char * sjoin(const char * delim, int count, char ** array) {
  char * result = NULL;
  int len = strlen(array[0])+1;
  result=(char*)malloc(len*sizeof(char));
  strcpy(result, array[0]);
  for (int i=1; i<count; ++i) {
    len+=strlen(array[i])+strlen(delim);
    result=(char*)realloc(result, len*sizeof(char));
    strncat(result, delim, strlen(delim));
    strncat(result, array[i], strlen(array[i]));
  }
  return result;
}
