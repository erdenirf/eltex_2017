#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

char ** (*ssplit) (const char *, const char *, int *);
char * (*sjoin) (const char *, int, char **);

int main(int argc, char *argv[]) {
  /* dlfcn.h begin */
  void *lib_handler = NULL;
  lib_handler=dlopen("./libdynamic.so", RTLD_LAZY);
  if (!lib_handler) {
    fprintf(stderr, "dlopen() error: %s\n", dlerror());
    exit(1);
  }
  ssplit = (char** (*)(const char *, const char *, int *))dlsym(lib_handler, "ssplit");
  sjoin = (char * (*)(const char *, int, char **))dlsym(lib_handler, "sjoin");
  /* dlfcn.h end */
  char str[]="Особенности национальной рыбалки - художественный, комедийный фильм.";
  char ** split = NULL;
  int size=0;
  printf("%s\n", str);
  split = ssplit(str, " ,.-", &size);
  char * join = NULL;
  for (int i=0; i<size; ++i) {
    printf("%s\n", split[i]);
  }
  join = sjoin("_", size, split);
  printf("%s\n", join);
  free(split);
  split = NULL;
  free(join);
  join = NULL;
  dlclose(lib_handler);
  return 0;
}
