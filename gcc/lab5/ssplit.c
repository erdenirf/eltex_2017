#include "ssplit.h"
#include <stdlib.h>
#include <string.h>

/*
 * Разбить строку на массив строк по указанной строке-разделителю
 */
char ** ssplit(const char * string, const char * delim, int * result_size) {
  char ** result = NULL;
  int asize = 0;
  char * temp = NULL;
  temp=(char*)malloc((strlen(string)+1)*sizeof(char));
  strcpy(temp, string);
  char * pch = strtok (temp, delim);
  while (pch != NULL) {
    ++asize;
    result=(char**)realloc(result, asize*sizeof(char*));
    result[asize-1]=(char*)malloc((strlen(pch)+1)*sizeof(char));
    strcpy(result[asize-1], pch);
    pch = strtok(NULL, delim);
  }
  free(temp);
  temp=0;
  *result_size = asize;
  return result;
}
