#include <stdio.h>
#include <stdlib.h>
extern char ** ssplit(const char *, const char *, int *);
extern char * sjoin(const char *, int, char **);

int main(int argc, char *argv[]) {
  char str[]="Особенности национальной рыбалки - художественный, комедийный фильм.";
  char ** split = NULL;
  int size=0;
  printf("%s\n", str);
  split = ssplit(str, " ,.-", &size);
  char * join = NULL;
  for (int i=0; i<size; ++i) {
    printf("%s\n", split[i]);
  }
  join = sjoin("_", size, split);
  printf("%s\n", join);
  free(split);
  split = NULL;
  free(join);
  join = NULL;
  return 0;
}
