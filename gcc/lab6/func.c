#include "func.h"
#include <stdlib.h>

/**
* malloc memory for matrix of template_t
**/
template_t** malloc_matrix(int rows, int columns) {
	template_t** mas = NULL;
	mas = (template_t**)malloc(rows*sizeof(template_t*));
	for (int i=0; i<rows; ++i) {
		mas[i] = (template_t*)malloc(columns*sizeof(template_t));
	}
	return mas;
}

/**
* free memory matrix of template_t
**/
void free_matrix(template_t** matrix, int rows) {
	for (int i=0; i<rows; i++) {
		free (matrix[i]);
		matrix[i] = NULL;
	}
	free(matrix);
	matrix = NULL;
}

/*
 * Считать матрицу из потока с указанными размерами
 */
void finp_matrix(FILE * stream, template_t ** matrix, int rows, int columns) {
  for (int i=0; i<rows; ++i) {
    for (int j=0; j<columns; ++j) {
      fscanf(stream, template_f, &matrix[i][j]);
    }
  }
}

/*
 * Вывести в поток матрицу с указанными размерами
 */
void fout_matrix(FILE * stream, template_t ** matrix, int rows, int columns) {
  for (int i=0; i!=rows; ++i) {
    for (int j=0; j!=columns; ++j) {
      fprintf(stream, template_f, matrix[i][j]);
      fprintf(stream, " ");
    }
    fprintf(stream, "\n");
  }
  fflush(stream);
}

/*
 * Считать вектор указанного размера из потока
 */
void finp_vector(FILE * stream, template_t * vector, int asize) {
  for (int i=0; i!=asize; ++i) {
    fscanf(stream, template_f, &vector[i]);
  }
}

/*
 * Вывести вектор на указанный выходной поток
 */
void fout_vector(FILE * stream, template_t * vector, int asize) {
  for (int i=0; i!=asize; ++i) {
    fprintf(stream, template_f, vector[i]);
    fprintf(stream, " ");
  }
  fprintf(stream, "\n");
  fflush(stream);
}

/*
 * Выделить память под вектор
 */
template_t * malloc_vector(int columns) {
  template_t * result = NULL;
  result = (template_t*)malloc(columns*sizeof(template_t));
  return result;
}

/*
 * Free memory for vector of template_t
 */
void free_vector(template_t * vector) {
  free (vector);
  vector = NULL;
}

/*
 * Умножение матрицы на вектор только указанную строку матрицы на строку вектора
 */
template_t mul_matrix_vector_by_row(int rows, int columns, template_t ** matrix, template_t * vector, int line) {
  if (line<0 || line>=rows) {
    fprintf(stderr, "Ошибка. Строка-линия умножения выходит за рамки памяти");
    exit(EXIT_FAILURE);
  }
  template_t result = 0;    /* sum */
  for (int i=0; i!=columns; ++i) {
    result+=matrix[line][i] * vector[i];
  }
  return result;
}

/*
 * Открыть файл и считать 1 значение заданного типа
 */
int fopen_and_read(char * filename, template_t * value) {
  FILE * tmp_in_pid = NULL;
  tmp_in_pid = fopen(filename, "r");
  if (tmp_in_pid == NULL) {
    fprintf(stderr, "%s not opened for read.", filename);
    return -1;   //fail
  }
  else {
    fscanf(tmp_in_pid, template_f, value);
    fclose(tmp_in_pid);
  }
  return 0;
}

/*
 * Открыть файл и записать 1 значение заданного типа
 */
int fopen_and_write(char * filename, template_t value) {
  FILE * tmp_out_pid = NULL;
  tmp_out_pid = fopen(filename, "w");
  if (tmp_out_pid == NULL) {
     fprintf(stderr, "%s not opened for write.", filename);
     return -1;   //fail
  }
  else {
    fprintf(tmp_out_pid, template_f, value);
    fclose(tmp_out_pid);
  }
  return 0;
}

