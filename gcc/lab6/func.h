#ifndef FUNC_H
#define FUNC_H
#include <stdio.h>

#define template_t double
#define template_f "%lf"

template_t ** malloc_matrix(int rows, int columns); /* Выделить память под матрицу с типом-шаблоном */
void free_matrix(template_t ** matrix, int rows); /* Очистить память матрицы */
template_t * malloc_vector(int columns); /* Выделить память под вектор */
void free_vector(template_t * vector); /* Очистить память из под вектора */
void finp_matrix(FILE * stream, template_t ** matrix, int rows, int columns); /* Считать матрицу из потока с указанными размерами */
void fout_matrix(FILE * stream, template_t ** matrix, int rows, int columns); /* Вывести в поток матрицу с указанными размерами */
void finp_vector(FILE * stream, template_t * vector, int asize); /* Считать вектор из потока с указанным размером */
void fout_vector(FILE * stream, template_t * vector, int asize); /* Вывести вектор с указанным размером на указанный выходной поток */
template_t mul_matrix_vector_by_row(int rows, int columns, template_t ** matrix, template_t * vector, int line); /* Умножение матрицы на вектор только указанную строку матрицы на строку вектора */
int fopen_and_read(char * filename, template_t * value); /* Открыть файл и считать 1 значение заданного типа */
int fopen_and_write(char * filename, template_t value); /* Открыть файл и записать 1 значение заданного типа */


#endif
