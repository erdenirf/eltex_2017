#include "func.c"
#include "fork.c"

#define INPUTF "in.txt"

int main(int argc, char * argv[]) {
  int M = 0;   /* rows */
  int N = 0;   /* columns */
  template_t ** matrix = NULL;
  template_t * vector = NULL;
  template_t * result_vector = NULL;
  FILE * in = fopen(INPUTF, "r");
  if (in == NULL) {
    fprintf(stderr, "Ошибка открытия файла %s\n", INPUTF);
    return EXIT_FAILURE;
  }
  else {
    fscanf(in, "%d%d", &M, &N);
    matrix = malloc_matrix(M, N);
    finp_matrix(in, matrix, M, N);
    vector = malloc_vector(N);
    finp_vector(in, vector, N);
    fclose(in);
  }
  fprintf(stdout, "\tArray with %d rows, %d columns:\n", M, N);
  fout_matrix(stdout, matrix, M, N);
  fprintf(stdout, "\tVector with size %d:\n", N);
  fout_vector(stdout, vector, N);
  /* fork begin */
  result_vector = my_fork(M, N, matrix, vector);
  /* fork end */
  printf("\tResult Array with size %d:\n", M);
  fout_vector(stdout, result_vector, M);
  free_matrix(matrix, M);
  free_vector(vector);
  free_vector(result_vector);
  return 0;
}
