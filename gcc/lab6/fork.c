#include "fork.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define BUFSIZE 255

/*
 * Запуск rows-штук дочерних процессов в цикле, в каждом из которых ищется строка умножения матрицы на вектор
 * Возвращает вектор, результат умножения матрицы на вектор
 */
template_t * my_fork(int rows, int columns, template_t ** matrix, template_t * vector) {
  int pid[rows], status, stat, i;
  template_t * result = NULL;
  result = malloc_vector(rows);
  for (i = 0; i != rows; ++i) {
        // запускаем дочерний процесс 
        pid[i] = fork();
        srand(getpid());
        if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
	  printf(" CHILD: Это %d процесс-потомок СТАРТ! PID=%d. У родителя PPID=%d\n", i, getpid(), getppid());
            result[i]=mul_matrix_vector_by_row(rows,columns,matrix,vector,i);
	    printf(" RESULT[%d] = ", i);
	    printf(template_f, result[i]);
	    printf("\n");
            printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i);
	    char filename[BUFSIZE];
	    strcpy(filename, "");
	    snprintf(filename, BUFSIZE, "/tmp/%d.txt", getpid());
	    fopen_and_write(filename, result[i]);
            exit(EXIT_SUCCESS); /* выход из процесс-потомока */
        }
    }
    // если выполняется родительский процесс
  printf("PARENT: Это процесс-родитель! PID=%d\n", getpid());
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i != rows; ++i) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  exitcode=%d\n", i, WEXITSTATUS(stat));
	    char filename[BUFSIZE];
	    strcpy(filename, "");
	    snprintf(filename, BUFSIZE, "/tmp/%d.txt", pid[i]);
	    fopen_and_read(filename, &result[i]);
        }
	if (pid[i] != status) {kill(pid[i],SIGKILL); printf("Thread #%d killed!\n",pid[i]);}
    }
  return result;
}
