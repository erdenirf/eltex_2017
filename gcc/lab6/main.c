#include "func.c"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>

#define INPUTF "in.txt"
#define BUFSIZE 100

int main(int argc, char * argv[]) {
  int M = 0;   /* rows */
  int N = 0;   /* columns */
  template_t ** matrix = NULL;
  template_t * vector = NULL;
  template_t * result_vector = NULL;
  FILE * in = fopen(INPUTF, "r");
  if (in == NULL) {
    fprintf(stderr, "Ошибка открытия файла %s\n", INPUTF);
    return EXIT_FAILURE;
  }
  else {
    fscanf(in, "%d%d", &M, &N);
    matrix = malloc_matrix(M, N);
    finp_matrix(in, matrix, M, N);
    vector = malloc_vector(N);
    finp_vector(in, vector, N);
    fclose(in);
  }
  fprintf(stdout, "\tArray with %d rows, %d columns:\n", M, N);
  fout_matrix(stdout, matrix, M, N);
  fprintf(stdout, "\tVector with size %d:\n", N);
  fout_vector(stdout, vector, N);
  result_vector = malloc_vector(M);
  int pid[M], status, stat, i;
  for (i = 0; i != M; ++i) {
        // запускаем дочерний процесс 
        pid[i] = fork();
        srand(getpid());
        if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
	  printf(" CHILD: Это %d процесс-потомок СТАРТ! PID=%d. У родителя PPID=%d\n", i, getpid(), getppid());
            result_vector[i]=mul_matrix_vector_by_row(M,N,matrix,vector,i);
	    printf(" RESULT[%d] = ", i);
	    printf(template_f, result_vector[i]);
	    printf("\n");
            printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i);
	    char filename[BUFSIZE];
	    strcpy(filename, "");
	    snprintf(filename, BUFSIZE, "/tmp/%d.txt", getpid());
	    fopen_and_write(filename, result_vector[i]);
            exit(EXIT_SUCCESS); /* выход из процесс-потомока */
        }
    }
    // если выполняется родительский процесс
  printf("PARENT: Это процесс-родитель! PID=%d\n", getpid());
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i != M; ++i) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  exitcode=%d\n", i, WEXITSTATUS(stat));
	    char filename[BUFSIZE];
	    strcpy(filename, "");
	    snprintf(filename, BUFSIZE, "/tmp/%d.txt", pid[i]);
	    fopen_and_read(filename, &result_vector[i]);
        }
	if (pid[i] != status) {kill(pid[i],SIGKILL); printf("Thread #%d killed!\n",pid[i]);}
    }
    printf("\tResult Array with size %d:\n", M);
    fout_vector(stdout, result_vector, M);
  free_matrix(matrix, M);
  free_vector(vector);
  free_vector(result_vector);
  return 0;
}
