import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Пишем свой TCP сервер
 */
public class TCPServer {

    private int port = 0;

    private ServerSocket serverSocket = null;

    /**
     * конструктор с параметром порт
     * @param port
     */
    public TCPServer(int port) {
        this.port = port;
    }

    /**
     * биндим сервер к порту
     */
    public void bind() {
        try {
            this.serverSocket = new ServerSocket(port);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * слушаем порт и создаем потоки (нити) для принятого из очереди соединения
     */
    public void listen() throws IOException {
        do {
            Socket socket = serverSocket.accept();
            ServerThread serverThread = new ServerThread(socket);
            serverThread.start();
        } while (true);
    }

    /**
     * приватный класс Поток
     */
    private class ServerThread extends Thread {

        private Socket socket = null;

        /**
         * Конструктор с параметром сокет, на вход должны подать прослушиваемый сокет Client сервера
         * @param socket
         */
        ServerThread(Socket socket) {
            this.socket = socket;
        }

        /**
         * метод старт(), запуск потока
         */
        public void run() {
            try {
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                final int MAX_HEADER_SIZE = 20000;
                byte byteArray[] = new byte[MAX_HEADER_SIZE];
                int read = dataInputStream.read(byteArray, 0, MAX_HEADER_SIZE);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.write(byteArray, 0, MAX_HEADER_SIZE);
                System.out.write(byteArray, 0, MAX_HEADER_SIZE);
                sleep(1000);
                dataInputStream.close();
                dataOutputStream.close();
                socket.close();
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

}
