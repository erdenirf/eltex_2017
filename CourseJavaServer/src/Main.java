import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        int port = 3335;

        TCPServer tcpServer = new TCPServer(port);

        tcpServer.bind();

        tcpServer.listen();

    }
}
