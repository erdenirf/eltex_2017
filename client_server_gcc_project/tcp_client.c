#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "shared_struct.h"

/* TCP Client Start. Return: unsigned int Socket File Descriptor, -1 on Error */
int tcp_client_start(const char * hostname, uint16_t port);
/* TCP Client Stop. Close opened socket file descriptor */
void tcp_client_stop(int sockfd);

int main(int argc, char *argv[])
{
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(1);
    }

    // извлечение порта
    int portno = atoi(argv[2]);
    
    int socketfd = tcp_client_start(argv[1], portno);
    if (socketfd < 0) {
        exit(2);
    }

    struct shared_struct message_send;
    message_send.T_sleep = 3;
    strcpy(message_send.random_string, "Erdeni Tsyrendashiev");
    message_send.length_random_string = strlen(message_send.random_string);

    send(socketfd, &message_send, sizeof(message_send), 0);

    tcp_client_stop(socketfd);

    return 0;
}

/**
 * Create AF_INET STREAM socket. Return: unsigned int Socket File Descriptor, -1 on Error
 */
int open_created_socket_tcp_inet() {
    // AF_INET     - сокет Интернета
    // SOCK_STREAM  - потоковый сокет (с установкой соединения)
    // 0 - по умолчанию выбирается TCP протокол
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // ошибка при создании сокета
    if (sockfd < 0) {
      perror("socket");
      return -1;
    }
    return sockfd;
}

/**
 * Connect AF_INET socket with same Host Name and port. Return 0 on success, -1 on Error.
 */
int connect_socket_tcp_inet(int sockfd, const char * hostname, uint16_t port) {
    struct sockaddr_in serv_addr;  // структура сокета сервера
    struct hostent * host_address = NULL;
    host_address = gethostbyname(hostname);  //получаем указатель на структуру хоста, откуда берем адрес хоста
    if (host_address == NULL) {
        perror("gethostbyname");
        return -1;
    }
    // заполенние структуры serv_addr
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    memcpy(&serv_addr.sin_addr.s_addr, host_address->h_addr, host_address->h_length);
    // установка порта
    serv_addr.sin_port = htons(port);
    
    // Шаг 2 - установка соединения 
    int code_connect = connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr));
    if (code_connect < 0) {
        perror("connect");
        return -1;
    }
    return 0;
}

/**
* TCP Client Start. Return: unsigned int Socket File Descriptor, -1 on Error
*/
int tcp_client_start(const char * hostname, uint16_t port) {    
    // Шаг 1 - создание сокета
    int my_sock = open_created_socket_tcp_inet();
    if (my_sock < 0) {
        return -1;
    }
    // Шаг 2 - подключение клиента к хосту
    int code_connect = connect_socket_tcp_inet(my_sock, hostname, port);
    if (code_connect < 0) {
        return -1;
    }
    return my_sock;
}

/**
* TCP Client Stop. Close opened socket file descriptor
*/
void tcp_client_stop(int sockfd) {
    close(sockfd);
}
