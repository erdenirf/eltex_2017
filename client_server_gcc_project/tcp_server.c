#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include "shared_struct.h"

/* Start TCP Server. Return: unsigned int Socket File Descriptor, -1 on Error */
int tcp_server_start(uint16_t portno, size_t query_size);
/* Get client socket from server accept. Return: unsigned int Socket File Descriptor, -1 on Error */
int open_socket_accept(int sockfd);
/* Socket low level read and write. Return: 0 on Success, -1 on Error. */
int read_write_socket_tcp(int sockfd);
/* TCP Server Stop. Close opened Socket File Descriptor */
void tcp_server_stop(int sockfd);

int main(int argc, char * argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Error. Argument port is not found. Usage: %s <port>\n", argv[0]);
    exit(1);
  }

  const size_t QUERY_SIZE = 5;
  int portno = atoi(argv[1]);

  int serverfd = tcp_server_start(portno, QUERY_SIZE);
  if (serverfd < 0) {
    exit(2);
  }

    while (1)  //цикл извлечения запросов на подключение из очереди
    {
      int client_fd = open_socket_accept(serverfd);
      if (client_fd < 0) {
        exit(3);
      }
      pid_t pid = fork();
      if (0 > pid) {
        perror("fork");
      }
      if (0 == pid) {  // child
        close(serverfd);
        read_write_socket_tcp(client_fd);
        exit(0);  /* child fork exit */
      }
      if (0 < pid) {   //parent
        close(client_fd);
      }
    } /* end while */
  tcp_server_stop(serverfd);
  return 0;

}

/**
 * Create AF_INET socket with same port. Return: unsigned int Socket File Descriptor, -1 on Error.
 */
int open_created_socket_tcp_inet() {
    // AF_INET     - сокет Интернета
    // SOCK_STREAM  - потоковый сокет (с установкой соединения)
    // 0 - по умолчанию выбирается TCP протокол
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // ошибка при создании сокета
  if (sockfd < 0) {
    perror("socket");
    return -1;
  }
  return sockfd;
}

/**
 * Bind AF_INET socket with same port. Return: 0 on Success, -1 on Error.
 */
int bind_socket_tcp_inet(int sockfd, uint16_t port) {
  // Шаг 2 - связывание сокета с локальным адресом
  struct sockaddr_in serv_addr; // структура сокета сервера
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    serv_addr.sin_port = htons(port);
    // вызываем bind для связывания
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      perror("bind");
      return -1;
    }
    return 0;
}

/**
 * listen socket with same query size
 */
void listen_socket(int sockfd, int query_size) {
  listen(sockfd, query_size);
}

/**
* Start TCP Server. Return: unsigned int Socket File Descriptor, -1 on Error
*/
int tcp_server_start(uint16_t portno, size_t query_size) {
  int serverfd = open_created_socket_tcp_inet();
  if (serverfd < 0) {
    return -1;
  }
  int bind_code = bind_socket_tcp_inet(serverfd, portno);
  if (bind_code < 0) {
    return -1;
  }
  listen_socket(serverfd, query_size);
  return serverfd;
}

/**
* TCP Server Stop. Close opened Socket File Descriptor
*/
void tcp_server_stop(int sockfd) {
  close(sockfd);
}
 
/**
* Socket low level read and write. Return: 0 on Success, -1 on Error.
*/
int read_write_socket_tcp(int sockfd) {
  struct shared_struct message_recv;
  int bytes_recv = read(sockfd, &message_recv, sizeof(message_recv));
  if (bytes_recv < 0) {
    perror("read");
    return -1;
  }
  fprintf(stdout, "%s\n", message_recv.random_string);

  return 0;
}

/**
 * Get client socket from server accept. Return: unsigned int Socket File Descriptor, -1 on Error
 */
int open_socket_accept(int sockfd) {
  struct sockaddr_in cli_addr; // структура сокета клиента
  socklen_t clilen;       // размер структуры
  int result_socket = 0;      // файловый дестриптор результата
  clilen = sizeof(cli_addr);
  result_socket = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (result_socket < 0) {
    perror("accept");
    return -1;
  }
  return result_socket;
}
