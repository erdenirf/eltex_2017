#ifndef MSG_H
#define MSG_H
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define MAX_SEND_SIZE 255

struct mymsgbuf {
        long mtype;
        char mtext[MAX_SEND_SIZE];
};

int msg_init(const char *pathname, int proj_id); /* Инициализация очереди сообщений */
void send_message(int qid, struct mymsgbuf *qbuf, long type, char *text); /* Отправить сообщений в очередь */
void read_message(int qid, struct mymsgbuf *qbuf, long type); /* Считать сообщение из очереди */

#endif
