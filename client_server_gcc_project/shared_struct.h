#ifndef SHARED_STRUCT_H_
#define SHARED_STRUCT_H_

/**
* Разделяемая структура
*/
#define MAX_SHARED_STRUCT_BUFFER_SIZE 2048

struct shared_struct {
	unsigned int T_sleep;	/* Время Т обработки сообщения */
	size_t length_random_string;	/* Длина случайно строки */
	char random_string[MAX_SHARED_STRUCT_BUFFER_SIZE];	/* Случайная строка */
};

#endif